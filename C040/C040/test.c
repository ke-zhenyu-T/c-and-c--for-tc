#define _CRT_SECURE_NO_WARNINGS 1
//printf函数的返回值
#include<stdio.h>
//int main()
//{
//	//int ret = printf("Hello world!");//\n也算字符
//	////printf("\n");
//	//printf("\n%d\n", ret);
//	printf("\n%d\n", printf("Hello world"));
//	return 0;
//}

//int main()
//{
//	printf("%d", printf("%d", printf("%d", 43)));
//	return 0;
//}
//学生基本信息输入
int main()
{
	int num = 0;
	float c_score = 0.0;//最好写小数，double类型也可以
	float math_score = 0.0;
	float eng_score = 0.0;
	scanf("%d;%f,%f,%f", &num, &c_score, &math_score, &eng_score);
	printf("The each subject score of  NO.%d is %.2f,%.2f,%.2f.\n", num, c_score, math_score, eng_score);
	return 0;
}