#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<assert.h>
//字符函数和字符串函数
//求字符串长度：strlen:给一个字符串的起始地址，从该地址向后数，一个个数，数到\0停止，结果为\0的个数
//注意，函数返回值为size_t，是无符号的整数，是unsigned int
//int main()
//{
//	if (strlen("abc") - strlen("abcdef") > 0)//3-6=-3,但无符号减无符号还是无符号数的，-3但是最高位没有符号位的话
//		//即-3的补码被解读为无符号数，被认为就是原码，即一个非常大的正数
//	//if (my_strlen("abc") - my_strlen("abcdef") > 0)
//	{
//		printf(">\n");
//	}
//	else
//	{
//		printf("<\n");
//	}
//	return 0;
//}
//
//
int my_strlen(const char* str)//＋const，函数更安全，更健壮
//int和size_t各有千秋吧，size_t也有bug的
{
	//1.计数器（count）版本
	int count = 0;
	assert(str != NULL);
	while (*str != '\0')
	{
		count++;
		str++;
	}
	return count;
	//2.递归版本
	//3.指针减指针版本
}
//
//int main()
//{
//	char arr[] = "abc";
//	//char arr[] = { 'a','b','c' };//没有\0,会继续往后数，结果为随机值
//	/*int len=strlen(arr);*///有\0,但数的是\0之前的个数，同时也说明必须有\0
//	int len = my_strlen(arr);//模拟实现一个strlen
//
//	printf("%d\n", len);
//
//	return 0;
//}

//长度不受限制的字符串函数：strcpy strcat strcmp
//strcpy:
//1.源字符串必须以'\0'结束
//2.会将字符串中的'\0'拷贝到目标空间
//3.目标空间必须空间足够大，能容纳下源字符串内容
//3.目标空间必须可改
//4.学会模拟实现
//int main()
//{
//	//char arr[20] = { 0 };
//	//char arr[20] = "##########";
//
//	//arr = "hello";//error,因为arr本就是首元素的地址，是地址，是个编号，是个常量来的，把hello放编号吗？不对，应该是把hello放到arr对应的空间里
//	//char* strcpy(char* destination, const char* source);目的地指针，原指针
//	char* p = "hello";//实际上是把h的地址放到p里去，p指向字符串
//	/*strcpy(arr,"hello");*///string copy,字符串拷贝
//	//strcpy也是把首字符h的地址传给sources地址，指向hello，把sources的hello拷贝到destination指向的arr去
//	//strcpy拷贝的原数据没有\0可以吗？
//	//char arr2[] = { 'a','b','c' };
//	//strcpy(arr, arr2);//\0也是终止拷贝的条件，没有\0是不行的，程序会挂的，会一直往后找\0,找到时已经越界访问很久了
//	//strcpy是会把\0一起拷贝过去的
//
//	//char arr[5] = "#####";
//	//char* p = "hello world";
//	//strcpy(arr, p);//放不下也会有问题，能打印出来，但程序会崩溃，确实把arr数组撑爆了
//	//所有要保证目标空间有足够大的空间保存拷贝过去的数据
//	//还有，目的地空间必须可修改，const *不行
//
//	char* str = "xxxxxxxxxxxxxxx";//常量字符串
//	strcpy(str, p);//不行，目标指针指向的是常量字符串，不可修改，程序会崩
//	
//	//printf("%s\n", arr);
//
//	return 0;
//}

//字符串追加：strcat
char* my_strcat(char* dest, const char* src)//strcat返回的是目标空间的起始地址
{
	char* ret = dest;
	assert(dest && src);
	//1.先找到目标字符串的\0,找到才能开始在\0处追加
	while (*dest)
	{
		dest++;
	}
	//此时dest指向\0
	//2.源数据追加过去，包含\0
	while (*dest++ = *src++)//*优先于++
	{
		;
	}
	return ret;//返回目标空间的起始地址
}
//int main()
//{
//	//char arr1[20] = "hello \0########";//要保证目标空间足够大，能够容纳追加的内容
//	char arr1[20] = "hello ";
//	//char* strcat(char* destination, const char* source);
//	//strcat(arr1,"world");//字符串追加（连接）
//	char arr2[] = "world";//追加到源字符的\0停止
//	//strcat(arr1, arr2);//这样写也ok
//	//从被追加字符串\0处追加，\0也会带过来，即被追加字符串必须有\0,且\0也会被带过去
//
//	//模拟实现：
//	//my_strcat(arr1, arr2);
//	//printf("%s\n", arr1);
//	printf("%s\n", my_strcat(arr1,arr2));//返回的是目标字符串的起始地址嘛
//	return 0;
//}

//自己给自己追加？

//int main()
//{
//	char arr[20] = "abcd";
//	strcat(arr, arr);//自己给自己追加会出问题
//	//原理上：arr:abcd\0,追加会把\0覆盖，追加了abcd，到了\0的地方，已经被覆盖了，不会停止，会继续追加，会继续找\0,会继续在arr追加，继续追加abcd，死循环
//	//即结束标志\0被覆盖掉了，无法结束了
//	//但是vs2019不知道为什么就又行了
//	printf("%s\n", arr);
//	return 0;
//}

//字符串比较函数strcmp,比较两字符串
//int main()
//{
//	char* p = "obc";
//	char* q = "abcdef";
//	//比较两字符串大小
//	//当然不行：
//	//if (p > q)//地址来的，先高地址后低地址,比较地址了
//	//{
//	//	printf(">\n");
//	//}
//	//else
//	//{
//	//	printf("<=\n");
//	//}
//	
//	//if("obc">"abcdef")//?也不能，这样就是没有把首字符地址分别放qp，是直接拿两受字符地址直接比较了,
//
//	//strcmp - 字符串比较大小,比较的不是长度，是对应字符的ASCII码值
//	//两个相等时，比到\0相等，就不比了，得出两字符串相等
//	//int strcmp(const char* str1, const char* str2);
//	//int ret = strcmp("abbb", "abq");//当第一个字符串和第二个字符串对应字符不相等时，字符1小于字符2返回负数，大于返回正数，等于返回0
//	int ret = strcmp("aqaa", "aab");
//	printf("%d\n", ret);//返回的具体数与字符ASCII相差多少无关，负数为-1,正数为1(vs编译器才这样，其他编译器不一定的，是负数、正数就可以了)，比出结果即停止，相等比下一对
//
//	//模拟实现strcmp
//	return 0;
//}
//模拟实现strcmp

int my_strcmp(const char* s1, const char* s2)
{
	assert(s1 && s2);
	while (*s1 == *s2)// \0时也跳出
	{
		if (*s1 == '\0')
		{
			return 0;
		}
		s1++;
		s2++;
	}
	//if (*s1 > *s2)
	//{
	//	return 1;
	//}
	//else
	//{
	//	return -1;
	//}
	return *s1 - *s2;//没有规定说必须负数就是-1，正数就是1，负数、正数就可以了
}
//int main()
//{
//	char* p = "abcdef";
//	char* q = "abq";
//	int ret = my_strcmp(p, q);
//	if (ret > 0)
//	{
//		printf("p>q\n");//p指向字符串大于q的
//	}
//	else if(ret < 0)
//	{
//		printf("p<q\n");
//	}
//	else
//	{
//		printf("p==q\n");
//	}
//	return 0;
//}
//总结：strcpy、strcat、strcmp与长度无关，找到\0才结束


//长度受限制的字符串函数介绍：strncpy strncat strncmp  工作长度给定
//strncpy
//char* strncpy(char* destination, const char* source, size_t num);num表示烤呗的字符个数，增加了长度的限制
//int main()
//{
//	char arr1[20] = "abcdefghi";
//	char arr2[] = "qwer";
//	//strncpy(arr1, arr2, 2);//只拷两个，只把ab覆盖掉，多了个参数，更安全了，相对安全了
//	//strcpy(arr1, arr2);//不会管放不放的下，全拷过去，其实不够安全，删掉第一行，会报错的
//	
//	strncpy(arr1, arr2, 6);//烤多了就当\0烤，一定会烤够给你的，先烤qwer\0,再补一个\0
//	printf("%s\n", arr1);//
//	return 0;
//}

//strncat:
//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[] = "world";
//	strncat(arr1, arr2, 10);//追加过了，遇到\0就会结束追加的，然后在末尾补上结束符\0
//	//hello world\0 然后结束
//	//若只追加3个，会主动补上\0
//	//和strncpy不一样，strncpy是嗯凑上\0拷贝过去
//	printf("%s\n", arr1);
//	return 0;
//}

//strncmp 比较n个字符
//int main()
//{
//	char* p = "abcdef";
//	char* q = "abcqwert";
//	//int ret = strcmp(p, q);
//	//int strncmp(const char* str1, const char* str2, size_t num);
//	//int ret = strncmp(p, q, 3);//只比较前三个
//	int ret = strncmp(p, q, 4);//只比较前四个，
//	//都是分出胜负就结束
//	//加了个数限制，更安全
//	printf("%d\n", ret);
//	return 0;
//}

//字符串查找：strstr strtok
//strstr()在一个字符串里找另一个字符串
//p139
char* my_strstr(const char* str1, const char* str2)//只是查找，不改，加const保险一点
{
	assert(str1 && str2);
	//逻辑比较复杂，要画图分析清楚
	const char* s1 = str1;//确保不改变内容，但s1代表谁的地址是可以改的
	const char* s2 = str2;
	const char* cp = str1;//str1是const修饰的，所有cp也要对类型，也要const
	
	//调用strncmp也ok哦

	if (*str2 == '\0')//找的是空字符串
	{
		return (char*)str1;//(str1被const修饰了，类型不太一样，要强制转化，const char*和char*不一样)
	}
	while (*cp)//指向的是字符串不是结束符，进一次是一次匹配
	{
		s1 = cp;//一步步推进
		s2 = str2;//重新比
		//while (*s1 == *s2)
		//但有问题：万一查完了怎么办
		while (*s1 && *s2 && (*s1 == *s2))//*s1为\0时表示原字符串被找完了，*s2为\0时str2也找完了
		//abcdef里找cdef 同时遇到\0,也要跳出来，不然越界访问了
		{
			s1++;
			s2++;
		}
		
		if (*s2 == '\0')//跳出后，此时时找到了的
		{
			return (char*)cp;//cp是const char*类型的
		}
		cp++;//记录比较的起始位置，步步推进
	}
	return NULL;
}
//以上为常规写法，库里的也是这样的

//KMP算法：字符串中找字符串的算法，字符串查找算法，效率更高《程序员编程艺术》

//int main()
//{
//	char arr1[] = "abbbcdef";
//	char arr2[] = "bbc";
//	//char arr2[] = "bcdq";
//	//在arr1中查找是否包含arr2数组
//
//	//const char* strstr(const char* str1, const char* str2);
//	//char* strstr(char* str1, const char* str2);
//	//char*ret=strstr(arr1,arr2);//如果找到，必定是找到第一次出现的位置，返回第一次出现的，也就是b的地址，找不到会返回空指针
//	//模拟实现：
//	char* ret = my_strstr(arr1, arr2);
//	if (ret == NULL)
//	{
//		printf("找不到\n");
//	}
//	else
//	{
//		printf("找到了：%s\n", ret);
//	}
//	return 0;
//}

//strtok:切割字符串
//zpw@bitedu.tech 要得到zpw  bitede  tech三个部分，就要切割
//char* strtok(char* str, const char* sep);sep参数是个字符串，定义了用在分隔符的字符集合，即@和.
//int main()
//{
//	char arr[] = "zpw@bitede.tech";
//	char* p = "@. ";//分隔符的字符集合，空格也可以当分隔符的，字符串自带\0,所有\0不能成为分隔符，否则会越界的，找到下个字符串的
//	char tmp[20] = { 0 };
//	strcpy(tmp, arr);
//
//	char* ret = NULL;
//	//ret=strtok(tmp,p);//也可以直接在这里写"@."，
//	////找到分隔符，会将其用\0结尾，即把@改成\0,再返回z的地址，
//	//// 第一个参数不为空指针时,函数找到第一个标记，改成\0,并且会记录\0的位置，
//	//// 当第一个参数为空指针时，下次再调用，会从上次保存的地方继续找标记，再改\0，再返回b的地址，再记录\0位置
//	////难受的是会把要切割的源字符串改掉，所有在使用strtok函数切分的字符串一般都是临时拷贝的内容，并且可以被修改
//	////但strtok调用一次只能找到一个标志
//	//printf("%s\n", ret);
//
//	//ret=strtok(NULL, p);//从记住的地址（\0）继续找
//	//printf("%s\n", ret);
//
//	//ret=strtok(NULL, p);
//	//printf("%s\n", ret);
//
//	//即strtok只有第一次调用时传指针，以后都是空指针
//	//如果有很多标记要找？
//	//找不到会返回空指针，可以用这个判断要不要继续找
//	for (ret = strtok(tmp, p); ret != NULL;ret=strtok(NULL,p))//for循环初始化部分只执行一次
//	//tmp只执行一次，NULL会一直执行到找完
//	//局部变量加上static，出了函数也不会销毁，显然strtok有用static
//	//但能用static不搞全局变量，用全局变量很鸡肋的
//	{
//		printf("%s\n", ret);
//	}
//	return 0;
//}

//错误信息报告：strerror  帮我们报错
//使用库函数时，调用库函数失败时，都会设置错误码
//int errno;//C语言有个全局错误码叫errno，只要调用函数发生错误，都会把错误码放进errno
//strerror会把错误码翻译成对应的错误信息
//char* strerror(int errnum);参数是整数，错误码，返回的是错误信息的字符串的首地址
//errno是全局的错误码，都能使用
#include<errno.h>
int main()
{
	//printf("%s\n", strerror(0));//没错的错误码加上0
	//printf("%s\n", strerror(1));
	//printf("%s\n", strerror(2));
	//printf("%s\n", strerror(3));
	//printf("%s\n", strerror(4));//调用函数被中断
	//printf("%s\n", strerror(5));

	//使用：
	FILE* pf = fopen("test.txt", "r");//以读的形式打开，若文件不存在就会打开失败
	//打开失败，则pf得到空指针
	if (pf == NULL)
	{
		printf("%s\n", strerror(errno));//把错误码对应的错误信息打印出来
		//No such file or directory 该项目下没有这个文件夹
		return 1;//代码不会往下走
	}
	fclose(pf);
	pf = NULL;
	return 0;
}	
//机器是不会骗人的，坚定地排查吧

//字符操作

//内存操作函数：memcpy memmove memset memcmp