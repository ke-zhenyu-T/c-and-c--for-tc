#define _crt_secure_no_warnings 1
#include<stdio.h>
#include<string.h>
//int main()
//{
//	char arr1[20] = { 0 };
//	char arr2[] = "abc";
//	//printf("%d", strlen(arr2));
//	strcpy(arr1,arr2);//函数的返回类型 函数名(函数参数)--自定义函数也是
//	printf("%s\n", arr1);
//	return 0;
//}
//自定义函数：
//函数的返回类型 函数名(函数参数)
//{
//	函数体（整个大括号）
//}
//找出两个整数的最大值
int get_max(int x, int y)//传值调用
{
	int z = 0;
	if (x > y)
		z = x;
	else
		z = y;
	return z;//返回z，即返回较大值
}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int max = get_max(a,b);//函数名起的有意义点
//	printf("max=%d\n", max);
//	return 0;
//}
//可以交换两个整型变量的内容

//传址调用
void swap(int* pa, int* pb/*形式参数--形参*/)//函数定义  交换了就可以，不需要返回，不需要返回类型-->写void,表示这个函数不反悔函数任何值，也不需要返回
{
	//形参只有被调用时才被分配空间，用完还给操作系统，生命周期只在进入函数开始，结束函数销毁，和局部变量一样，只在函数内有效
	//通过赋值调换，地址不换的，原地址存放的数不变
	//为a和b创建了新的独立空间x，y，而借助z把xy交换来交换去，不会影响a和b的
	//压根不是同一个空间
	//这个代码有问题了-->注释掉吧-->地址-->指针
	int z = *pa;
	*pa = *pb;
	*pb = z;
	//该函数类型是不返回，可以写return，但不能跟东西，也就是不能返回东西
}//函数内外部发生联系时，才需要用地址
//指针
//int main()
//{
//	int a = 10;//四个字节的空间
//	int* pa = &a;//pa就是一个指针变量
//	*pa = 20;
//	printf("%d", a);
//	return 0;
//}
int main()
{
	int a = 10;
	int b = 20;
	int max = get_max(2+4, get_max(4,7));
	//函数的参数可以是变量、常量、表达式、函数,但必须得有能确定算出的值
	printf("max=%d\n", max);
	//写函数交换两整形变量的值
	printf("交换前:a=%d b=%d\n", a, b);
	swap(&a, &b/*实际参数，调用函数时，真是传输过去函数的*/);//函数调用  
	
	printf("交换后:a=%d b=%d\n", a, b);//会创造新的储存空间 
	return 0;
}
//函数被调用时，实参穿给形参，其实形参时实参1的一份临时拷贝，改变形参，不能改变实参