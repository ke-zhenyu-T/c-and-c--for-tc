#define _CRT_SECURE_NO_WARNINGS 1
//函数的return只能返回一个数据
//要返回多个数据，可以形参用数组，全局变量也行尽管不建议 
//test(int arr[])
//{
//	arr[0] = 1;
//	arr[1] = 2;
//}
//int main()
//{
//	int arr[10] = { 0 };
//	test(arr);//通过形参把值带回来了，或者直接传地址，不用数组1也可以
//	return 0;
//}
//函数可以嵌套调用，不能嵌套定义
//主调函数和被调函数不一定同一文件，声明一下就可以用了
//库函数有限，自定义函数更重要，自定义函数的使用很重要

//逗号隔开的为逗号表达式-->从左向右进行计算，最后的结果为最后一个表达式的结果即(v1,v2)==v2
//int i=1,j=2不是逗号表达式
#include<stdio.h>
//int main()
//{
//	int v1 = 1;
//	int v2 = 9;
//	printf("%d\n", (v1, v2));//打印v2
//	return 0;
//}
//函数打印乘法口诀表，自己决定行列
//void print_table(int n)
//{
//	int i = 0;
//	for (i = 1; i <= n; i++)//行
//	{
//		int j = 0;
//		for (j = 1; j <= i; j++)//列
//		{
//			printf("%d*%d=%-2d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);//9
//	//函数
//	print_table(n);//打印表--函数名字最好能体现功能
//
//	return 0;
//}
//递归作业
//int Fun(int n)
//{
//	if (n == 5)
//		return 2;
//	else
//		return 2 * Fun(n + 1);
//}
//int main()
//{
//	printf("%d", Fun(2));//-->16
//	return 0;
//}
//栈区：放局部变量，函数参数（形参），调用函数时的返回值等临时变量
//堆区：动态内存分配
//静态区：放全局变量，静态变量(sattic int num =0)
//栈-->指栈空间，栈区，数字结构的栈
//堆栈-->还是栈
//堆-->才是堆区
//每次函数调用都会在栈开辟空间：main,Fun,Fun,fun,Fun,
//但返回，归时，空间会返还给栈，一步步销毁

//字符串逆序(不能用函数库，如：strlen
//非递归
//int my_strlen(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//void reverse_string(char* str)//写成指针和写成数组是一样的
//{
//	int left = 0;
//	int right =my_strlen(str) - 1;
//	while(left<right)
//	{ 
//	//char tmp = str[left];
//	/*str[left] = str[right];
//	str[right] = tmp;*/
//	char tmp = *(str + left);//说明指针和数组是基本一样的
//	*(str + left) = *(str + right);
//	*(str + right) = tmp;
//	left++;
//	right--;
//	}
//}
//int main()
//{
	//char arr[] = "abcdef";
	//reverse_string(arr);//数组名arr是数组首元素的地址
	//
	//printf("%s\n", arr);//fedcba
//	return 0;
//}
//递归版-->大事化小
//int my_strlen(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//void reverse_string(char* str)
//{
//	char tmp = *str;//数组首元素a的地址就是数组名
//	int len = my_strlen(str);
//	*str = *(str + len - 1);//第二步
//	*(str + len - 1) = '\0';//第三步
//	//判断条件
//	if(my_strlen(str+1)>=2)//不然只剩一个字母，还逆什么序
//	reverse_string(str+1);//第四步
//	*(str + len - 1) = tmp;//第五步
//
//}
//int main()
//{
//	//a b c d e f \0
//	char arr[] = "abcdef";
//	reverse_string(arr);
//
//	printf("%s\n", arr);
//	return 0;
//}
//计算一个数的每位之和
//int DigitSum(int n)//大事化小
//{
//	if (n > 9)
//	{
//		return DigitSum(n / 10) + n % 10;
//	}
//	else
//	{
//		return n;
//	}
//}
//int main()
//{
//	int num = 1729;
//	int sum = DigitSum(num);
//	printf("%d\n", sum);
//	return 0;
//}
//递归实现n的k次方
double Pow(int n, int k)
{
	//Pow(n,k)=1,k=0
	//Pow(n,k)=n*Pow(n,k-1),k>0
	if (k == 0)
		return 1.0;
	else if (k > 0)
		return n * Pow(n, k-1);/*严重性	代码	说明	项目	文件	行	禁止显示状态
	警告	C4715	“Pow” : 不是所有的控件路径都返回值	C039	D : \code\c - and -c--for - tc\C039\C039\test.c	170*/
	//考虑不全k<0没考虑
	else
		return 1.0 / (Pow(n, -k));
}
int main()
{
	int n = 0;
	int k = 0;
	scanf("%d %d", &n, &k);
	double ret = Pow(n, k);
	printf("%lf\n", ret);
	return 0;
}