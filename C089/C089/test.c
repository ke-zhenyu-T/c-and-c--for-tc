#define _CRT_SECURE_NO_WARNINGS 1
//atoi的实现 把字符串转换为整形的函数
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<ctype.h>
#include<limits.h>

//int main()
//{
//	char* p = "-1234";
//	int ret = atoi(p);
//	printf("%d\n", ret);
//	return 0;
//}
//来自《剑指offer》



//enum State
//{
//	INVALID,//0
//	VALID//1
//};
////state 记录的时my_atoi 返回的值时合法转换的值，还是非法的状态
//
//enum State state=INVALID;
//
//int my_atoi(const char* s)
//{
//	int flag = 1;
//	//assert(NULL != s);或：
//	if (s == NULL)
//	{
//		return 0;//返回0是表示转换结果为0还是表示非法呢？
//		
//	}
//	//空字符串
//	if (*s == '\0')
//	{
//		return 0;
//	}
//	//跳过空白字符：空格，tab，遇到一个挑一个
//	while (isspace(*s))
//	{
//		s++;
//	}
//	//+-正负号问题
//	if (*s == '+')
//	{
//		flag = 1;
//		s++;
//	}
//	else if (*s == '-')
//	{
//		flag = -1;
//		s++;
//	}
//	//处理数字字符的转换
//	//int n = 0;//能放进去的就已经符合范围了，已经截断了，没办法判断
//	long long n = 0;
//	while (isdigit(*s))
//	{
//		n = n * 10 + flag*(*s - '0');//超出范围怎么办？怎么判断呢
//		if (n > INT_MAX || n < INT_MIN)
//		{
//			//return (int)n;
//			return 0;
//		}
//		s++;
//	}
//	if (*s == '\0')
//	{
//		state = VALID;
//		return (int)n;
//	}
//	else//数字后面有非数字字符，停止转换并且返回
//	{
//		state = VALID;
//		return (int)n;
//	}
//}
//int main()
//{
//	//1.空指针 
//	//2.空字符串
//	//const char* p = "-1234";
//	//int ret = my_atoi(p);
//	//char* p = NULL;
//	//char* p = "";
//	char* p = "     -122a2222";//很多异常情况：遇到非数字字符，超出范围超出整形可以表达的数字
//	int ret = my_atoi(p);
//	if (state == VALID)
//		printf("正常转换：%d\n", ret);
//	else
//		printf("非法的转换：%d\n", ret);
//	
//	return 0;
//}
//把整形转化为字符串：sprintf()、
//scanf:从字符串中提取整形

//_itoa函数也是有的，不过不是标c提供的，可能是vs单独提供的

//找单身狗进阶版
//一个数组中只有两个数字是出现一次，其他数组都出现两次
//1 2 3 4 5 6 1 2 3 4

void Find(int arr[], int sz,int*px,int*py)
{
	//1.把所有数组异或
	int i = 0;
	int ret = 0;
	for (i = 0; i < sz; i++)
	{
		ret ^= arr[i];
	}
	//2.计算ret的哪一位为1
	//ret==3 011
	int pos = 0;
	for (i = 0; i < 32; i++)
	{
		if ((ret >> i) & 1 == 1)
		{
			pos = i;
			break;
		}
	}
	//把从低位向高的第pos为为1的放在一个组，为0的放在另外一个组
	int num1 = 0;
	int num2 = 0;
	for (i = 0; i < sz; i++)
	{
		if (((arr[i] >> pos) & 1) == 1)
		{
			num1 ^= arr[i];
		}
		else
		{
			num2 ^= arr[i];
		}
		
	}
	*px = num1;
	*py = num2;
}
int main()
{
	int arr[] = { 1,2 ,3 ,4 ,5 ,6 ,1 ,2 ,3 ,4 };
	//分两组1 3 1 3 5
	//      2 4 2 4 6
	// 分组变成只有一个数字出现一次，能很好的各自抑或找出来
	//1.分组
	//2.5和6必须在不同组：要点：不同组
	//
	//找出这两个只出现一次的数字
	//1^2^3^4^5^6^1^2^3^4=5^6=3 看3的哪一位为1，既哪一位不同，就拿这位来分组，若不止一个1，就选一位有1的就可以了， 相同两数字异或的结果为0，0和任何数异或为自己
	//5^6结果必不为0，且肯定有1，相同为0、异为1
	//法一：-->最低位为0的为1组，为1的为另一组
	//1 3 5 1 3
	//2 2 4 4 6
	//法二：第二位为0为一组，为1为另一组
	//1 4 1 4 5
	//2 2 3 3 6
	//保证每个组只有一个数组出现一次
	int sz = sizeof(arr) / sizeof(arr[0]);
	int x = 0;//传进去x、y的地址
	int y = 0;//叫返回型参数
	//传数组也行:
	//int diff[2];
	Find(arr, sz,&x,&y);
	printf("%d %d\n", x, y);
	return 0;
}