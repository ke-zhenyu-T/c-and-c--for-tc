#define _CRT_SECURE_NO_WARNINGS 1

#include"contact.h"

void InitContact(Contact* pc)
{
	pc->sz=0;
	//memset() - 内存设置
	//void* memset(void* dest, int c, size_t count);以字节为单位进行初始化
	memset(pc->data,0,sizeof(pc->data));//pc->data相当于数组名，即首元素地址
}

void AddContact(Contact* pc)
{
	if (pc->sz == MAX)
	{
		printf("通讯录已满，无法添加\n");
		return;//void不需要返回值，return就能结束
	}
	//增加一个人的信息
	printf("请输入名字:>\n");
	scanf("%s", pc->data[pc->sz].name);
	printf("请输入年龄:>\n");
	scanf("%d", &(pc->data[pc->sz].age));
	printf("请输入性别:>\n");
	scanf("%s", pc->data[pc->sz].sex);
	printf("请输入电话:>\n");
	scanf("%s", pc->data[pc->sz].tele);
	printf("请输入地址:>\n");
	scanf("%s", pc->data[pc->sz].addr);

	pc->sz++;
	printf("增加成功\n");

}
void PrintContact(const Contact* pc)
{
	int i = 0;
	//先打印标题		
	printf("%-20s\t%-5s\t%-5s\t%-12s\t%-20s\n", "名字", "年龄", "性别", "电话", "地址");
	//加符号左对齐，不加是右对齐
	//再打印数据
	for (i = 0; i < pc->sz; i++)
	{
		printf("%-20s\t%-5d\t%-5s\t%-12s\t%-20s\n", pc->data[i].name,
													pc->data[i].age,
													pc->data[i].sex,
													pc->data[i].tele,
													pc->data[i].addr);
	}
}

static int FindByName(Contact*pc,char name[])//static修饰函数时，函数只能在自己源文件内部看到，其他源文件不能用
{
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		if (strcmp(pc->data[i].name, name) == 0)
		{
			return i;
		}
	}
	return -1;//找不到
}

void DelContact(Contact* pc)
{
	char name[MAX_NAME] = { 0 };
	if (pc->sz == 0)
	{
		printf("通讯录为空，无需删除\n");
		return;
	}
	printf("请输入要删除人的名字:>");
	scanf("%s", name);
	//1.查到人才能删（查找要删除的人）（还有个功能也是查找，可以一起调用）
	//有没有
	int pos=FindByName(pc,name);//找到就返回下标，找不到就返回-1
	if (pos == -1)
	{
		printf("要删除的人不存在\n");
		return;
	}
	//2.删除
	//把要删除的地点从后面往前挪覆盖掉就是删除了
	int i = 0;
	for (i = pos; i < pc->sz-1; i++)//最后一个可以不覆盖，因为之后再录入也会覆盖掉的，避免越界访问
	{
		pc->data[i] = pc->data[i + 1];
	}
	pc->sz--;
	printf("删除成功\n");
}

void SearchContact(Contact* pc)
{
	char name[MAX_NAME] = { 0 };
	printf("请输入要查找人的名字:>");
	scanf("%s", name);
	int pos = FindByName(pc, name);
	if (pos == -1)
	{
		printf("要查找的人不存在\n");
		return;
	}
	else
	{
		printf("%-20s\t%-5s\t%-5s\t%-12s\t%-20s\n", "名字", "年龄", "性别", "电话", "地址");
		//加符号左对齐，不加是右对齐
		//再打印数据
		printf("%-20s\t%-5d\t%-5s\t%-12s\t%-20s\n", 
			pc->data[pos].name,
			pc->data[pos].age,
			pc->data[pos].sex,
			pc->data[pos].tele,
			pc->data[pos].addr);


	}
}

void ModifyContact(Contact* pc)
{
	//1.首先是查找
	char name[MAX_NAME] = { 0 };
	printf("请输入要修改人的名字:>");
	scanf("%s", name);
	int pos = FindByName(pc, name);//一个函数可以复用的
	if (pos == -1)
	{
		printf("要修改的人不存在\n");
		return;
	}
	else
	{
		//2.重录一遍
		printf("请输入名字:>\n");
		scanf("%s", pc->data[pos].name);
		printf("请输入年龄:>\n");
		scanf("%d", &(pc->data[pos].age));
		printf("请输入性别:>\n");
		scanf("%s", pc->data[pos].sex);
		printf("请输入电话:>\n");
		scanf("%s", pc->data[pos].tele);
		printf("请输入地址:>\n");
		scanf("%s", pc->data[pos].addr);
		printf("修改成功\n");
		//修改不影响人数，sz不动
	}

}