#define _CRT_SECURE_NO_WARNINGS 1
//调试技巧
//int main()
//{
//	int i = 0;
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	for (i = 0; i <= 12; i++)
//	{
//		arr[i] = 0;
//		printf("hello bit\n");
//	}
//	return 0;
//}
//调整奇数偶数顺序,奇数在前偶数在后
#include<stdio.h>
void print(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}
void move(int arr[],int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left < right)
	{
		//从前往后找一个偶数
		while ((left<right)&&(arr[left] % 2 == 1))
		{
			left++;
		}
		//从后往前找一个奇数
		while ((left < right) && (arr[right] % 2 == 0))
		{
			right--;
		}
		if (left < right)
		{
			int tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;
		}
	}
}
int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };//其实有问题
	//int arr[] = { 1,3,5,7,9 };//若全是奇数，left一直加加，会造成越界访问，全偶也会，所以必须加上left<right的前提条件
	int sz = sizeof(arr) / sizeof(arr[0]);
	move(arr,sz);//调整函数
	print(arr, sz);
	return 0;
}