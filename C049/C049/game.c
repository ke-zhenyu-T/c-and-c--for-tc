#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"
void menu()
{
	printf("*******************\n");
	printf("***** 1.play  *****\n");
	printf("***** 0.exit  *****\n");
	printf("*******************\n");
}
void InitBoard(char board[][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			board[i][j] = ' ';//二维数组赋值
		}
	}
}

void DisplayBoard(char board[ROW][COL], int row, int col)//显示棋盘
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			printf(" %c ", board[i][j]);
			if (j < col - 1)
			{
				printf("|");
			}
		}
		printf("\n");
		if (i < row - 1)
		{
			for (j = 0; j < col; j++)
			{
				printf("---");
				if (j < col - 1)
					printf("|");
			}
		printf("\n");
		}
	}
}
void PlayrMove(char board[ROW][COL], int row, int col)//玩家走
{
	int x = 0;
	int y = 0;
	printf("玩家走:>\n");
	while (1)
	{
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			if (board[x - 1][y - 1] == ' ')
			{
				board[x - 1][y - 1] = '*';
				break;
			}
			else
			{
				printf("坐标被占用，请重新输入");
			}
		}
		else
		{
			printf("非法坐标，请重新输入");
		}
	}
}
void ComputerMove(char board[ROW][COL], int row, int col)//电脑走
{
	printf("电脑走:>\n");
	while (1)
	{
		int x = 0;
		int y = 0;
		x = rand() % row;
		y = rand() % col;
		if (board[x][y] == ' ')
		{
			board[x][y] = '#';
			break;
		}
	}
}
int IsFull(char board[ROW][COL], int row, int col)//判断棋盘是否已满
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			if (board[i][j] == ' ')//满了没
				return 0;
		}
	}
	return 1;
}
char IsWin(char board[][COL], int row, int col)//判断胜负平
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][1] != ' ')
		{
			return board[i][0];
		}
	}
	for (i = 0; i < col; i++)
	{
		if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != ' ')
		{
			return board[0][i];
		}
	}
	if ((board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != ' ') || (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' '))
	{
		return board[1][1];
	}
	int ret = IsFull(board, row, col);
	if (ret == 1)
		return 'Q';
	return 'C';
}
void game()//游戏函数
{
	char board[ROW][COL];
	char ret = 0;
	InitBoard(board, ROW, COL);
	DisplayBoard(board,ROW,COL);//显示
	while (1)
	{
		PlayrMove(board, ROW, COL);//玩家走
		DisplayBoard(board, ROW, COL);
		ret = IsWin(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
		ComputerMove(board, ROW, COL);//电脑走
		DisplayBoard(board, ROW, COL);
		ret = IsWin(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
	}
	if (ret == '*')
		printf("玩家赢\n");
	else if (ret == '#')
		printf("电脑赢\n");
	else
		printf("平局\n");
	DisplayBoard(board,ROW,COL);
}