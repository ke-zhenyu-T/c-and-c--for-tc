#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define ROW 3
#define COL 3
void InitBoard(char board[ROW][COL], int row, int col);//二维数组赋值，营造下棋位置
void DisplayBoard(char board[ROW][COL], int row, int col);//营造并显示棋盘
void PlayerMove(char board[][COL], int row, int col);//玩家走棋
void ComputerMove(char board[ROW][COL], int row, int col);//电脑走棋
char IsWin(char board[ROW][COL], int row, int col);//判断输赢
int IsFull(char board[ROW][COL], int row, int col);//判断棋盘是否已满
