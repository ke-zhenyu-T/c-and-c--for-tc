#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//int main()
//{
//	printf("hello,World");
//	return 0;
//}
//调试技巧
//Bug--计算机程序或硬件中存在的缺陷--臭虫
//调试--找bug的过程就是调试
//拒绝迷信式调试法
//科学调试：
//调试：Debugging/Debug--发现和减少计算机程序或电子设备中程序错误的一个过程
//基本步骤：
//1.发现程序错误的存在
//  程序员自己发现--自己解决
//  软件测试人员--测试软件
//  用户发现--严重
//void test()
//{
//	int a = 10;
//	int b = 20;
//	int c = a + b;
//}
//int main()
//{
//	int arr[10] = { 0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		arr[i] = i + 1;
//	}
//	for (i = 0; i < sz; i++)
//	{
//		test();
//		printf("%d\n", arr[i]);
//	}
//	return 0;
//}
//Debug-调试版本--内存大小相对较大：包含一些调试信息，且不做任何优化，便于程序员调试 
//Realease-发布版本--较小，会自主对代码优化，是最优的，无法调试，测试人员测试的就是Release版本
//调试时调回Debug

//windows环境调试
//windows调试准备
//快捷键：本电脑都要+Fn
//F5开始、启动调试 F9设置或取消断点--程序执行到这里就会断开
//F10逐过程，一行一行调试
//F11可以进入函数，进入之后也会一项项，进入函数内部后等于F10，即更加细粒度
//ctrl+F5,执行不调试，无视断点

//调试的时候查看相关信息
//内存中以二进制储存，但以十六进制展示，更清楚
//调用堆栈反应函数调用逻辑

//void test2()
//{
//	printf("hehe");
//}
//void test1()
//{
//	test2();
//}
//void test()
//{
//	test1();
//}
//int main()
//{
//	test();
//	return 0;
//}

//void test()
//{
//	int a = 10;
//	int b = 20;
//	int c = a + b;
//}
//int main()
//{
//	int arr[10] = { 0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		arr[i] = i + 1;
//	}
//	for (i = 0; i < sz; i++)
//	{
//		test();
//		printf("%d\n", arr[i]);
//	}
//	return 0;
//}
//调试实例一:求1！+2！+n!
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	int ret = 1;
//	int sum = 0;
//	int j = 0;
//	for (j = 1; j <= n; j++)
//	{
//		ret = 1;
//		for (i = 1; i <= j; i++)
//		{
//			ret *= i;
//		}
//		sum += ret;
//	}
//	printf("%d\n", sum);
//	return 0;
//}
//不是语法错误，是运行时的错误，调试解决的就是运行时的错误
//解决问题：
//应该是什么结果-要有预期   调试时发现不符合预期，就找到问题了
//首先推测原因，大概推测，然后调试
//实例二：
//int main()
//{
//	int i = 0;
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	for (i = 0; i <= 12; i++)//越界访问了
//	{
//		arr[i] = 0;
//		printf("hehe\n");
//	}
//	//代码运行的结果是什么？--死循环--因为arr和i是俩局部变量，先创建i，再创建arr，又因为局部变量放在栈区，栈区使用习惯，先使用高地址再使用低地址，又因为数组随着下标增长，地址由高到低变化，所以数组用下标访问时，只要适当往后越界，就又可能覆盖到i，而只要i变化，就有可能导致程序的死循环
//	//出自《c陷阱和缺陷》--c语言学到三分之二时可以看
//	return 0;
//}
//x86版本：i和arr[12]地址相同，当arr[12]赋值为0，即i赋值为0，则会陷入死循环
//VS里，给i分配的空间地址在数组后面后移两个int位置
//i和arr是局部变量，局部变量是放在栈区上的，规定：栈区内存的使用习惯是先使用高地址空间，再使用低地址空间，反过来定义就没事了，会报错了，不循环了，先定义arr，在定义i，arr都在i上面，arr依次向上
//这里的栈区和上一节数据结构的栈是不一样的，数据结构的栈是先进后出，先瓶底后瓶口
//为什么只有12不报错？也越界呀。因为死循环根本没机会报错
//VC6.0环境，arr和i紧挨着的，中间空0个整形
//gcc中间空1个整形
//vs2013-2019 空2个整形
//-->巧合
//推荐书籍：《明解c语言》分初级和进阶，进阶以练习为主，《c和指针》，《比特c课间》，《c语言深度解剖》，c primer plus太厚，容易放弃，谭浩强-通俗易懂，但代码风格很差，可以看语法，不模仿代码
//F5会跳到程序执行中逻辑上的下一个断点

//预防发生错误：通过编码的技巧，减少错误
//模拟实现strcpy-字符串拷贝
#include<string.h>
//void my_strcpy(char*dest,char*src)//但写得不好
//{
//	while (*src!='\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest = *src;
//}
//void my_strcpy(char* dest, char* src)
//{
//	while (*src != '\0')
//	{
//		*dest++ = *src++;//++后置
//	}
//	*dest = *src;
//}
#include<assert.h>
//void my_strcpy(char* dest,const char* src)//继续简洁
//{
//	assert(src!=NULL);//断言--如果满足什么不允许发生什么的时候就会报错
//	//条件为真，什么也不发生，为假，则报错,报出错误这个信息，src==NULL
//	//断言断的是判断条件，在第几行，能迅速定位问题地方
//	
//	while (*dest++ = *src++)// \0的ASCII码值就是0，既拷贝了\0,又使得循环停止，一赋值就停下来了
//	//如果dest和src写反了，会崩溃，arr1太长了，放arr2会越界访问，崩溃，-->用到const
//	//while(*src++=*dest++)//用了const，编码都过不去，const，表达式必须是可修改的左值
//	{
//		;
//	}//也有局限性，万一destination位置传了个空指针，直接解应用，会越界访问报错的,空指针不能进行解应用操作
//}//还有三个点能优化
//原码：
//char* strcpy(char* destination, const char* source);多了个const
//const:1.修饰变量，该变量称为常变量，不能被修改，但是本质是仍是变量
//int main()
//{
//	//int num = 10;
//	//int* p = &num;
//	//*p = 20;
//	//printf("%d\n", num);
//	//新要求：num不让改
//	const int num = 10;
//	int n = 100;
//	//num = 20;//常变量不能被改， 不能进门
//	//int* p = &num;
//	//const int* p = &num;//const修饰指针变量时，如果放在*左边，修饰的时*p，修饰的是指针指向的内容，表示指针指向的内容不能通过指针改变
//	//若const在*右边？修饰的是指针变量p，表示指针变量不能被改变，但是指针指向的内容可以随意改变
//	int* const p = &num;//离p近，p不能改了，但*p又能改了，没限制了
//	*p = 20;//仍然能改，why? 翻窗了-->那就把窗也关了
//	//p = &n;//修改了p变量本身，const修饰了*p，但指针变量本身p仍然能改变
//
//	printf("%d\n", num);
//	return 0;
//}
//还有 int const*p=&m 和const int*p=&m是一样的
//int const * const p，即p被修饰了，*p也被修饰了，凉皮女孩既吃不到凉皮，也换不到男朋友
//二级指针 int const * const * const p,分别修饰p，*p，**p



//strcpy是把src指定的内容拷贝放进dest指向的空间中
//从本质上讲，希望dest指向的内容被需改，src指向的内容不修改
//char* strcpy(char* destination, const char* source);为什么返回char*
//strcpy这个库函数其实返回的是目标空间的起始地址

//char* my_strcpy(char* dest, const char* src)//const防止写反，写反直接报错，语法错误能很快找到
//{
//	assert(src != NULL);
//	assert(dest != NULL);
//	char* ret = dest;
//	while (*dest++ = *src++)//读到\0,ASCII码值为0，结果为假，既完成了拷贝，也完成了停止
//	{
//		;
//	}
//	//return dest;//?? 不行，dest已经++过了，已经不是原来的地址了
//	return ret;
//}	
//
//
//int main()
//{
//	int a = 3;
//	int b = 5;
//	int c = a = b + 3;//c=8,a=8,b=5
//	char arr1[20] = "xxxxxxxxx";
//	char arr2[] = "hello";// \0要拷贝吗,拷贝的
//	//strcpy(arr1,arr2);//把arr2的hello拷贝放到arr1
//	printf("%s\n", my_strcpy(arr1, arr2));//printf只读到\0,遇到\0才停止，这就是链式访问，\p是打印地址的，\s是打印字符串的
//	return 0;
//}

//strlen 是求字符串长度,模拟实现my_strlen
//int my_strlen(const char*p)
//{
//	assert(p != NULL);
//	int count = 0;
//	while(*p++ != '\0')
//		count++;
//	return count;
//}

//声明外部符号
extern size_t my_strlen(const char* str);

int main()
{
	char ch[] = "hehe";
	char*p = &ch;
	printf("%d", my_strlen(p));
	//printf("%d",mystrlen(p));//严重性	代码	说明	项目	文件	行	禁止显示状态
	//错误	LNK2019	无法解析的外部符号 mystrlen，函数 main 中引用了该符号	C059	D : \code\c - and -c--for - tc\C059\C059\test.obj	1

	return 0;
}
//让代码更加健壮--健壮性（鲁棒性）--代码皮实--不能改，有const修饰
//标答
//size_t-->unsigned int-->无符号整形
//size_t my_strlen(const char* str)
//{
//	//assert(str != NULL);or
//	assert(str);//NULL空为假，也报错
//	int count = 0;
//	while (*str++ != '\0')
//	{
//		count++;
//	}
//	return count;
//}//源代码用的是指针减指针
//int main()
//{
//	char arr[] = "abc";
//	//int len = strlen(arr);
//	int len=my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}
//_cdecl -函数调用约定--决定函数调用里的一些细节和规则
//总结编程常见错误：
//1.编译型错误：少符号等，会报错，双击一般能定位错误地点，多为语法错误
//2.链接性错误：无法解析的外部符号，没定义也会，或定义了但名字写错了也会,即在链接时出的错，跨文件，双击无反应
//	这种错误要么这个符号不存在，要么写错了，照符号去找就是了
//有警告是因为用了外部函数--声明一下--extern size_t my_strlen(const char* str)
//3.运行时错误，程序能跑但错误了，结果不对，要借助于调试，逐步解决问题
//可以高一些错题本之类的，积累排错经验，也可以写排错经验的博客




//2.以隔离和消除的方法对错误进行定位
//3.确定错误产生的原因
//4.提出纠正错误的解决方法
//5.对程序错误予以改正，重新测试
 
































//void test2()
//{
//	printf("hehe");
//}
//void test1()
//{
//	test2();
//}
//void test()
//{
//	test1();
//}
//int main()
//{
//	test();
//	return 0;
//}