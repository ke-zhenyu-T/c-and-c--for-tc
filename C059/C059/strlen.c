#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
int my_strlen(const char*p)
{
	assert(p != NULL);
	int count = 0;
	while(*p++ != '\0')
		count++;
	return count;
}
