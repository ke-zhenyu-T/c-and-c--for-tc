#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//int cnt = 0;
//int fib(int n)
//{
//	cnt++;
//	if (n == 0)
//		return 1;
//	else if (n == 1)
//		return 2;
//	else
//		return fib(n - 1) + fib(n - 2);
//}
//void main()
//{
//	fib(8);
//	printf("%d\n", cnt);
//}
//递归，看不出，就用树状图一步步倒，数，一共67次
//int main()
//{
//	int i = 10;
//	int j = 20;
//	int k = 3;
//	k *= i + j;//+的优先级高于*=
//	printf("%d\n", k);
//	return 0;
//}
//#include<stdlib.h>
//int a = 1;
//void test()
//{
//	int a = 2;//与上面的全局变量a名字相同时，局部优先
//	a += 1;
//}
//int main()
//{
//	test();
//	printf("%d\n", a);
//	return 0;
//}
//c语言只规定了各种语法规则，库函数是利用c语言的语法写出的，库函数是不属于c语言的一部分，是c语言的一种补充，c语言是没有输入输出语句的
//c语言每个厂商的实现都不一样，只规定了参数，返回类型，功能，不管怎么实现的
//test.c            -->                  test.exe 可用程序
//预处理（预编译）编译  汇编  链接
//预编译时将注释删除，编译检测语法错误

//3和2是两个整数，所以3/2=1,也是整数
//int main()
//{
//	int x = 0;
//	int y = 0;
//	for (x = 0, y = 0; (y = 123) && (x < 4); x++)//共循环4次
//		;
//	return 0;
//}
//++的优先级高于*解应用 *p++,+的是地址，不是地址里的内容
//字符乘整型时，会发生整形提升，把字符类型提升为整型，ASCII码，即向更精确的兼容

//最小公倍数
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
	//int m = a > b ? a : b;;//最小公倍数
	//while (1)简单但慢
	//{
	//	if (m % a == 0 && m % b == 0)
	//	{
	//		printf("%d\n", m);
	//		break;
	//	}
	//	m++;
	//}
//	int i = 1;
//	for (i = 1;; i++)//while写也可以的
//	{
//		if (a * i % b == 0)//模就是看看能不能整除 
//		{
//			printf("%d\n", a * i);
//			break;
//		}
//	}
//	return 0;
//}

//倒置字符串，将一句话的单词倒置，以空格分割并输出
void reverse(char*left,char*right)
{
	while (left<=right)
	{
		char tmp = 0;
		tmp = *left;
		*left = *right;
		*right = tmp;
		left++;
		right--;
	}
}
int main()
{
	char arr[100] = { 0 };
	//scanf("%s", arr);//这种方法做不到的,因为scanf读到空格就结束了，后面的输入就没了，输入abc def录进去abc
	//printf("%s", arr);
	//输入进来了
	gets(arr);//I like beijing
	//gets读取时不读\n
	//三步翻转法
	//1.字符串整体反转gnijieb ekil I
	int len = strlen(arr);
	reverse(arr,arr+len-1);
	printf("%s\n", arr);
	//2.每个单词逆序  找空格
	//beijing like I
	char* start = arr;
	while (*start)// \0的ASCII码值是0
	{
		//找空格
		char* end = start;
		while (*end != ' ' && *end != '\0')
		{
			end++;
		}
		//逆序一个单词
		reverse(start, end - 1);
		if (*end == ' ')
			start = end + 1;
		else
			start = end;
	}
	printf("%s\n", arr);
	return 0;
}

