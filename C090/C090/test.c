#define _CRT_SECURE_NO_WARNINGS 1
//程序的编译（预处理操作）+链接 作业

//宏替换
#include<stdio.h>

//#define N 4
//#define Y(n) ((N+2)*n)
//int main()
//{
//	int z = 2 * (N + Y(5 + 1));
//	//替换后int z = 2 * (4 + ((4 + 2) * 5 + 1));
//
//	printf("%d\n", z);//70
//	
//	return 0;
//}

//#define A 2+2
//#define B 3+3
//#define C A*B
//int main()
//{
//
//	printf("%d\n", C);//`11
//	return 0;
//}

////多个源文件同时用到的全局变量，它的声明和定义都放在头文件里，是不好的
//#include"add.h"
//int g_val = 100;//包含一次头文件相当于调用了定义
//
//#include"add.h"//万一不小心多次包含，即重复定义了，在多个源文件同时作用的情况下，有时候无法避免多次包含
//int g_val = 100;

//所以像函数的定义、变量的定义，不要放头文件
//重定义报错的
//头文件的包含、类型的定义声明、函数的声明放头文件里
//声明可以多次，定义不可以

//交换奇偶数：写一个宏，可以将整数的二进制位和奇数位互换

#define SWAP(N) ((N & 0xaaaaaaaa)>>1) + ((N & 0x55555555)<<1)
//单片机编程用的多是这些二进制编程、二进制位运算
//int main()
//{
//	//10
//	//00000000000000000000000000001010 假设最低位是奇数位，相邻交换-->0101=5
//	//相当于偶数位右移一位，奇数位左移一位
//	//11111111111111111111111111
//	//拿偶数位：按位与上：
//	//10101010101010101010101010
//	//拿奇数位：按位与上：
//	//01010101010101010101010101
//
//	int num = 10;
//	//int ret=((num & 0xaaaaaaaa)>>1) + ((num & 0x55555555)<<1);
//	//移位再相加
//	int ret = SWAP(num);
//	printf("%d\n", ret);
//	return 0;
//}

//offsetof宏的实现(offsetof不是函数，是宏)
//写一个宏，计算结构体中某变量相对于首地址的偏移
#include<stddef.h>
struct A
{
	int a;//0-3
	short b;//4-5
	//6-7浪费
	int c;//8-11
	char d;//12
};
//#define OFFSETOF(struct_name,mem_name) (int)&(((struct_name*)0)->mem_name)//把0强转为结构体指针，再抽成员出来，只是想知道偏移量，哪怕0不是所设结构体的地址，步长合适，算的就是偏移量而已，可以的，别忘了最后转回整型
//相当于在0处地址放了个结构体，因为是结构体加箭头可以模拟访问成员，没有真的拿、改变该空间的东西，强转不会开辟空间，得到成员地址，又从0开始，地址就是偏移量，其实就是省去减去起始地址的步骤
#define OFFSETOF(struct_name,mem_name) (int)&(((struct_name*)0x400)->mem_name)-0x400//这样也OK的

int main()
{
	//size_t offsetof(structName, memberName);//参数：结构体名，成员名
	//计算一个结构体成员相对于起始位置的偏移量
	//printf("%d\n", offsetof(struct A, a));
	//printf("%d\n", offsetof(struct A, b));
	//printf("%d\n", offsetof(struct A, c));
	//printf("%d\n", offsetof(struct A, d));
	printf("%d\n", OFFSETOF(struct A, a));
	printf("%d\n", OFFSETOF(struct A, b));
	printf("%d\n", OFFSETOF(struct A, c));
	printf("%d\n", OFFSETOF(struct A, d));
	return 0;
}