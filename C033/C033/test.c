#define _CRT_SECURE_NO_WARNINGS 1
//输出printf函数的返回值
//printf()返回打印的字符的个数
#include<stdio.h>
//int main()
//{
//	//int ret=printf("Hello world!");/*有无\n的返回值是不一样的*/
//	////printf("\n");
//	//printf("\n%d\n", ret);
//	//再简化：
//	printf("\n%d\n", printf("Hello world!"));
//	return 0;
//}
int main()
{
	int ret = printf("ab");
	printf("%d\n", ret);
	return 0;
}