#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//习题：
//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) != EOF)//如果等于EOF证明读取失败
//	{
//		int i = 0;
//		for (i = 0; i < n; i++)
//		{
//			int j = 0;
//			for (j = 0; j < n; j++)
//			{
//				if (i == j)
//					printf("*");
//				else if (i + j == n - 1)
//				{
//					printf("*");
//				}
//				else
//					printf(" ");
//			}
//			printf("\n");
//		}
//	}
//	return 0;
//}

//打印图案往往找规律
//int main()
//{
//	int sum = 0;
//	int score = 0;
//	int i = 0;
//	int max = 0;
//	int min = 100;
//	for (i = 0; i < 7; i++)
//	{
//		scanf("%d", &score);
//		sum += score;
//		if (score > max)
//			max = score;
//		if (score < min)
//			min = score;	
//		//好的代码，一次循环，既读取了值，又求了和，又求了最小值，有求了最大值
//		//若写三个循环分别读取、求最值，谁都写得出，太low了
//
//	}
//	printf("%.2f\n", (sum - max - min) / 5.0);
//	//练多了就会了
//	return 0;
//}

//获得月份天数
int main()
{
	int y = 0;
	int m = 0;
	int days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	while (scanf("%d %d", &y, &m) != EOF)
	{
		int day = days[m];
		if (m == 2)
			if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
			{
				day += 1;
			}
		printf("%d\n", day);
	}
	return 0;
}