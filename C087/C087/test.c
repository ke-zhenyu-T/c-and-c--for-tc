#define _CRT_SECURE_NO_WARNINGS 
//预处理：
//1.预定义符号:在预处理阶段被处理的已定义好的符号

//__STDC__;如果编译器遵循ANSI C，其值为1，否则未定义
#include<stdio.h>
//int main()
//{
	//printf("%s\n", __FILE__);//打印正在进行编译的源文件的路径和名字
	//
	//printf("%d\n", __LINE__);//获取该代码所在的行
	//printf("%s\n", __DATE__);//打印文件被编译时的日期
	//printf("%s\n", __TIME__);//获取文件被编译时的时间
	//printf("%s\n", __FUNCTION__);//获得所在函数的函数名
	//若调试时无从下手，则可在程序走时记录些日志信息，然后进行排查
	//正常运行时也应该记录日志的

	//int i = 0;
	//FILE* pf = fopen("log.txt", "a+");//a+是追加模式
	//if (pf == NULL)
	//{
	//	perror("fopen\n");
	//	return;
	//}
	//for (i = 0; i < 10; i++)
	//{
	//	fprintf(pf, "%s %d %s %s %d\n", __FILE__, __LINE__, __DATE__, __TIME__, i);
	//}
	//fclose(pf);
	//pf = NULL;

	//printf("%d\n", __STDC__);//找不到，不支持
//	return 0;
//}

//#define 预处理中的#define主要是用于定义符号、定义宏
//定义符号：
//#define M 1000//预处理阶段时就会把M替换为1000
//#define reg register//定义的不一定是数字
//#define do_forever for(;;)//也可以定义一段代码
//int main()
//{
//	int m = M;//这里就没有M了，只有1000
//	reg int num = 0;
//	do_forever;//死循环
//	printf("%d\n", m);
//	return 0;
//}

#define CASE break;case
//int main()
//{
//	int n = 0;
//	switch (n)
//	{
//	case 1:
//	CASE 2:
//	CASE 3:
//		//就不需要主动加break了
//	//	break;
//	//case 2:
//	//	break;
//	//case 3:
//	//	break;
//	}
//	return 0;
//}
//还要续航符\   在P175
#define M 1000;//理论上可加分号，但实际不好
//int main()
//{
//	int m = M;//实际上变成int m=1000;;
//
//	//也有可能出错
//	int a = 10;
//	int b = 0;
//	if (a > 10)
//		b = M;
//	//实际上是b=1000;
//	//;
//	//则if后面跟了两条语句，else不知道和谁匹配了，报错了
//	//-->能不加分号就不加
//	else
//		b = -M;
//}

//#define定义宏
//#define机制包括一规定，允许把参数替换到文本中，这种实现通常称为宏或定义宏
//#define name(parament-list) stuff  名字，参数，定义到的内容
//参数列表的左括号必须与name紧邻

#define SQUARE(X) X*X
//宏的定义的本质还是完成替换
//#define DOUBLE(X) (X)+(X)
#define DOUBLE(X) ((X)+(X))
//int main()
//{
//	//printf("%d\n", SQUARE(3));
//	//本质上是printf("%d\n", 3 * 3);
//	//printf("%d\n", SQUARE(3+1));
//	printf("%d\n", 10 * DOUBLE(4));
//	//实际上是printf("%d\n", 10 * (4) + (4));因为没有整体宏定义时，结果没有整体带括号
//	//实际上是3+1*3+1==7  -->完全替换 预编译阶段完全不计算的
//	//如果想整体传输-->从宏入手，改为#define SQUARE(X) ((X)*(X)),顺带把整个宏的结果也整体括号，稳妥点
//
//	return 0;
//}
//-->#define定义宏时不要吝啬括号，能给就给

//#define替换规则： 看P175吧
// 注意：
//1.宏参数和#define定义中可以出现其他#define定义的常量，但对于宏，不能出现递归
//2.当预处理器搜索#define定义的符号时，字符串常量的内容并不被搜索
//
//#define M 100
//#define MAX(X,Y) ((X)>(Y)?(X):(Y))
//int main()
//{
//	int max = MAX(101, M);
//	printf("M = %d\n", M);//M = %d\n在字符串里，这里的M不会被搜索到的
//	return 0;
//}

//#和##
//宏是替换，#是把参数插入到字符串中
void print(int x)
{
	printf("the value of who? is %d\n", x);
}
//int main()
//{
//	//printf("hello world\n");
//	//printf("hello " "world\n");//其实是一样的
//	
//	int a = 10;
//	//打印the value of a is 10
//	int b = 20;
//	//打印the value of b is 10
//	int c = 30;
//	//打印the value of c is 10
//	//三个打印很相似，只有名字和值不一样，三个printf冗余
//	//函数完成很难
//
//	return 0;
//}
//那就用宏实现
//#define PRINT(X) printf("the value of "#X" is %d\n",X);//加上#，就不是替换了，会变成X这个内容所对应的字符串
#define PRINT(X,FORMAT) printf("the value of "#X" is "FORMAT"\n",X);//加上#，就不是替换了，会变成X这个内容所对应的字符串
//int main()
//{
//	//printf("hello world\n");
//	//printf("hello " "world\n");//其实是一样的
//
//	int a = 10;
//	//打印the value of a is 10
//	int b = 20;
//	//打印the value of b is 10
//	int c = 30;
//	//打印the value of c is 10
//	PRINT(a,"%d");
//	//替换后printf("the value of "a" is %d", a);//有语法错误
//	//+个#号，表插入
//	//替换后printf("the value of ""a"" is %d", a);//变成三个字符串了
//	PRINT(b,"%d");
//	PRINT(c,"%d");
//
//	float f = 5.5f;
//	PRINT(f, "%f");
//	//替换后printf("the value of ""f"" is ""%f""\n", f);
//	return 0;
//}
//#不能在printf用，只能在宏里用

//更奇葩的##，可以两个符号连成一个符号
//#define CAT(X,Y,Z) X##Y##Z
//int main()
//{
//	int class101101 = 100;
//	//printf("%d\n", CAT(class, 101));//##会把class和101组合成一部分变成class101
//	printf("%d\n", CAT(class, 101,101));
//	//把两个符号class、101连接在一起
//	//三个符号也能合
//	return 0;
//}
//在原码里可能读到，我们实际很少写

//带副作用的宏参数
//int a = 1;
//int b = a + 1;//b=2,a=1，无副作用
//如果换成int b = ++a;//b=2,a=2
//则称++a是有副作用的
//宏的参数带副作用：
//#define MAX(X,Y) ((X)>(Y)?(X):(Y))
//int main()
//{
//	int a = 5;
//	int b = 8;
//	int m = MAX(a++, b++);
//	//被替换成int m = ((a++) > (b++) ? (a++) : (b++));
//	//后置++，5>8为假，结果为b++，比完a变成6，b加加了两次，变成10，但因为后置，所以b的第二次加加是在打印之后，所以打印9，但b变成了10
//	printf("m=%d\n", m);//结果是9
//	printf("a=%d b=%d", a, b);
//
//	return 0;
//}
//少使用带有副作用的参数
//当宏参数在宏定义中出现超过一次时，如果参数带有副作用，则你使用这个宏时可能会出现危险，导致不可预测的后果，副作用加上表达式求值时出现的永久性效果、产生遗留效果

//宏和函数的对比
//#define MAX(X,Y) ((X)>(Y)?(X):(Y))
//int MaX(int x, int y)
//{
//	return x > y ? x : y;
//}
//int main()
//{
//	int a = 5;
//	int b = 8;
//	//int m = MAX(a, b);//用宏
//	//用宏，在预处理就被替换为
//	int m = ((a) > (b) ? (a) : (b));
//	//执行时也就是这句话了，没调用什么的
//	m = MaX(a, b);//用函数,就是这句话
//	//int m = MAX(a++, b++);
//	////被替换成int m = ((a++) > (b++) ? (a++) : (b++));
//	////后置++，5>8为假，结果为b++，比完a变成6，b加加了两次，变成10，但因为后置，所以b的第二次加加是在打印之后，所以打印9，但b变成了10
//	//printf("m=%d\n", m);//结果是9
//	//printf("a=%d b=%d", a, b);
//
//	//选择宏，效率更高：
//	//1.用于调用函数和从函数返回的代码可能比实际执行这个小型计算工作所需的时间更多
//	//所以宏比函数在程序的规模和运行速度上更胜一筹
//	//在汇编阶段，函数的调用和返回有大量工作，所耗时间可能比运算还多
//	//宏，只有运算罢了，小型计算宏有优势
//	//2.函数，只能求设定好的固定类型的大小，本体只能算整形
//	//而宏只需能比较就可以了，宏是没有参数类型检查的，能适合各种类型，更加灵活
//
//	//但宏也有劣势：每次调用宏，则一份宏的定义会插入程序中（只要是宏，就会替换代码，代码和原来不一样了），除非宏比较短，否则可能大幅增加代码的长度
//	//函数定义好后都会去执行，不会变代码
//	//且宏无法调试的：调试是代码经过编译、链接后，生成可执行程序后，在运行时调试的
//	//宏在预处理阶段就替换了，在可执行时与看到的是不一样了，替换过了，无法调试
//	//不在乎参数类型也有不严谨的缺点
//	//也可能带来运算符优先级的问题，导致程序出错，而函数参数只在函数调用时求值一次，它的结果传递给函数，表达式的求值结果更容易预测
//
//	return 0;
//}
//当然，有时候函数做不到的，宏可以做得到（如#插入）
//宏ok函数不ok的例子：互补
//#define MALLOC(num,type) (type*)malloc(num*sizeof(type))
//int main()
//{
//	//malloc(10 * sizeof(int));//会不会有点麻烦
//	//malloc(10, int);//这样不行，函数不支持，且没见过传参传类型的
//	int*p=MALLOC(10, int);//自己定义一个宏
//	//(int*)malloc(10 * sizeof(int));//替换后
//	return 0;
//}
//宏和函数各自优缺点表格：P177 30minute前后

//宏不能递归，函数可以

//如果一个运算的功能足够简单，可以用宏，因为其简单，所以写起来不太容易错，带有副作用的参数可能不太影响，也因为简单，所以代码不会太长
//如果运算规律、逻辑比较复杂，就用函数吧，更容易控制和预测
//因为当函数的调用和返回的时间与运算相比微不足道时，宏和函数的效率区别不大

//内联函数inline 结合了宏的优点和函数的优点

//命名约定：一般来时函数和宏使用语法很相似，所以为了分辨：
//宏名全部大写，函数名不要全部大写
//有时候如果想把一个宏伪装成函数，就全小写

//#undef
//#define是定义什么，#undef是取消定义
//#define M 100
//int main()
//{
//	int a = M;
//	printf("%d\n", M);
//#undef M//取消define定义的符号，移出一个宏定义
//	return 0;
//}

//命令行定义： -D      用Linux演示
//即相当于传参数进去，在命令行里传参数进去、或定义
//现在少用，以前单片机内存小，一开始数组空间不敢给太大，给个小的，先不定义，后来用到再在命令行插入

//windows演示：还是不行的
//int main()
//{
//	int arr[M] = { 0 };
//	int i = 0;
//	for (i = 0; i < M; i++)
//	{
//		arr[i] = i;
//	}
//	for (i = 0; i < M; i++)
//	{
//		printf("%d ", i);
//	}
//	return 0;
//}

//条件编译：满足条件就编译，不满足就不编译
//比如调试性的代码，删除又可惜，保留又碍事，所以可以选择性编译
//#define PRINT
//int main()
//{
//#ifdef PRINT //如果这个符号PRINT被定义了，就会编译，没定义，不编译
//	printf("hehe\n");
//#endif
//	return 0;
//}
//像个开关一样，定义了就编译，不满足就不编译
//常见的条件编译指令：
//1.
//#if 常量表达式
//     //...
//#endif //#endif总是用来结束
//#define PRINT 1
//int main()
//{
////#if 1-2
//#if PRINT
//	printf("hehe\n");
//#endif
//	return 0;
//}
//炫技：注释
#if 0
一段废话，相当于注释掉了，相当于删了
#endif

//2.多分支条件编译
//#if 常量表达式
//#elif 常量表达式
////...
//#else
////...
//#endif
//int main()
//{
//#if 1==1
//	printf("hehe\n");
////#elif 1==2
//#elif 2==2//前面条件为真了，哪怕后面也为真也不会编译了，前面为假才会轮到后面
//	printf("haha\n");
//#else 
//	printf("heihei\n");
//#endif
//}

#if 0
//3.判断是否被定义
#define TEST
//#define HEHE
int main()
{

#ifdef TEST//如果TEST定义，下面代码参与编译，定义的值不重要，有定义即可
	printf("test\n");
#endif

//如果想要HEHE不定义，下面参与编译
#ifndef HEHE
	printf("hehe\n");
#endif

//还有一种
#if defined(TEST)//和#ifdef TEST写法等效的
	printf("test2\n");
#endif

#if !defined(HEHE)//和#ifndef HEHE等效
	printf("hehe2\n");
#endif
	return 0;
}
//1、3等效，2、4等效，1、2相反、3、4相反
//判断是否定义过某个符号
#endif

//翻译stdio.h
//int main()
//{
//	EOF;//跳转到stdio.h头文件里有很多类似代码
//	return 0;
//}

//条件编译指令可以嵌套：
//嵌套指令：可以在条件内部代码进行各种各样嵌套 P177 72minute


//文件包含 #include 是个预处理指令，可以另外一个文件被编译
//#include"filename"//包含本地头文件
#include"add.h"//""对本地文件的包含,自定义的函数的头文件使用
#include<stdio.h>//<>是对库文件的包含，包含C语言库里提供的函数的头文件使用<>

//<>和""包含头文件本质区别是：查找策略的区别
//查找策略：<>形式是先在源文件所在的目录下查找，如果该头文件未找到，编译器就像查找库函数一样在标准位置查找头文件
//"":1.自己代码所在的目录底下查找；2.如果第1找不到，则在库函数的头文件目录下查找
//<>:直接去库目录底下查找（库函数头文件所在的目录）
//为了效率，库的就用<>,自己的，万不得已，就用""
//本机vs2019标准库在D:\Windows Kits\10\Include\10.0.19041.0\ucrt
//linux放在/usr/include目录下
//int main()
//{
//	EOF;
//	int a = 10;
//	int b = 20;
//	int ret=Add(a, b);
//	printf("%d\n", ret);
//	return 0;
//}

//嵌套文件包含
//写代码时可能出现头一个头文件被重复多次包含
//在预处理里如果被包含两次，就会把代码拷贝进去两次
//如何做到只包含一份？-->linux演示-->法一：#pragma once
//或:
//P179 20minute
//法二：
#ifndef __TEST_H__
#define __TEST_H__
//头文件
#endif
//两种方法都能防止一个头文件被多次重复包含

//预处理指令：
//#include
//#define
//#if
//#ifdef
//#endif
//#ifndef
//#else
//#elif
//#undef

//#pragma once
//#pragma pack//设置默认对齐数的
//#pragma comment

//蛋哥 - 《C语言深度解剖》