#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//int get_max(int x, int y)//传值调用
//{
//	int z = 0;
//	if (x > y)
//		z = x;
//	else
//		z = y;
//	return z;
//}
//void swap1(int x,int y)//传值调用
//{
//
//	int z = x;
//	x = y;
//	y = z;
//}
//void swap2(int* pa, int* pb)//传址调用
//{
//	
//	int z = *pa;
//	*pa = *pb;
//	*pb = z;
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int max = get_max(2 + 4, get_max(4, 7));
//	
//	printf("max=%d\n", max);
//	swap1(a,b);//穿值调用--改变形参值不会影响实参
//	printf("交换前:a=%d b=%d\n", a, b);
//	swap2(&a, &b);//穿址调用--将外部函数变量创建地址传输至内部，可让函数与函数外部的变量建立联系，即函数内部可直接操作外部的变量
//
//	printf("交换后:a=%d b=%d\n", a, b);
//}
//判断一个数是否素数
#include<math.h>
int is_prime(int n)
{
	//2-->n-1之间得数字
	int j = 0;
	for (j = 2; j < sqrt(n); j++)
	{
		if (n % j == 0)
			return 0;//只能返回一个值，但不同情况，不会同时返回
	}
	return 1;
}
//int is_prime(int n)
//{
//	//2-->n-1之间得数字
//	int j = 0;
//	for (j = 2; j < n; j++)
//	{
//		if (n % j == 0)
//		{
//			printf("%d 不是素数\n", n);
//			return 0;
//		}
//	}	
//	printf("%d 是素数\n", n);
//	return 1;
//}//每次都打印，太繁琐，不够独立，很low


//int main()
//{
//	//100-200得素数
//	int i = 0;
//	int count = 0;
//	for (i = 100; i < 201; i++)
//	{
//		//判断是否素数
//		if (is_prime(i) == 1)
//		{
//			count++;	
//		}
//		
//	}
//	printf("\ncount=%d\n ",count);
//	return 0;
//}
//打印闰年
int is_leap_year(int n)
//一个函数如果不适用返回类型，默认返回int
//void是写了，但是代表不返回，不需要返回
{
	//是闰年则返回1
	//if ((n % 4 == 0) && (n % 100 != 0))
	//{
	//	return 1;
	//}
	//else if (n % 400 == 0)
	//{
	//	return 1;
	//}
	////不是则返回0
	//else
	//	return 0;
	//更简洁写法
	return ((n % 4 == 0 && n % 100 != 0) || (n % 400 == 0));
	//return (真)-->返回1
	//return (假)-->返回0
}
int main()
{
	int y = 0;
	for (y = 1000; y < 2001; y++)
	{
		if (is_leap_year(y) == 1)
		{
			printf("%d ", y);
		}
	}
	return 0;
}