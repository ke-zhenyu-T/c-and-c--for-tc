#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//P130
//指针进阶的作业：

//int(*(*F)(int,int))(int)
//定义一个函数指针，指向的函数有两个int形参数并且返回一个函数指针，返回的指针指向一个有一个int形参数且返回int的函数

//int main()
//{
//	char str1[] = "hello bit";//数组是开辟空间的
//	char str2[] = "hello bit";
//	char* str3 = "hello bit";//常量字符串,存一份就可以了，不开辟新空间
//	char* str4 = "hello bit";
//	if (str1 == str2)
//		printf("str1 and str2 are same\n");
//	else
//		printf("str1 and str2 are not same\n");
//	if (str3 == str4)
//		printf("str3 and str4 are same\n");
//	else
//		printf("str3 and str4 are not same\n");
//
//	return 0;
//}

//杨氏矩阵
//有一个数字矩阵，矩阵每行从左到右递增，从上到下递增，编写程序在作业的矩阵中查找某个数字是否存在
//要求：时间复杂度小于o(N)
//1 2 3 4
//2 3 4 5
//4 5 6 7 这就是一个杨氏矩阵

//int find_num(int arr[3][3], int r, int c, int k)//带坐标怎么办
//{
//	//定位右上角的坐标
//	int x = 0;//上下 x行
//	int y = c - 1;//左右 y列
//	while (x < r && y >= 0)
//	{
//		if (arr[x][y] < k)
//		{
//			x++;
//		}
//		else if (arr[x][y] > k)
//		{
//			y--;
//		}
//		else
//		{
//			return 1;//找到了
//		}
//	}
//	return 0;//找不到了
//}

//int find_num(int arr[3][3], int *px, int* py, int k)//带坐标怎么办
//{
//	//定位右上角的坐标
//	int x = 0;//上下 x行
//	int y = *py - 1;//左右 y列
//	while (x < *px && y >= 0)
//	{
//		if (arr[x][y] < k)
//		{
//			x++;
//		}
//		else if (arr[x][y] > k)
//		{
//			y--;
//		}
//		else
//		{
//			*px = x;//找到k的位置，带回来坐标信息
//			*py = y;
//			return 1;//找到了
//		}
//	}
//	return 0;//找不到了
//}
//
//int main()
//{
//	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
//	//查找一个数字，比如说找：7
//	//遍历？
//	//int i = 0;
//	//int j = 0;
//	//for (i = 0; i < 3; i++)
//	//{
//	//	for (j = 0; j < 3; j++)
//	//	{
//	//		if (arr[i][j] == 7)
//	//		{
//
//	//		}
//	//	}
//	//}
//	//能解决，但时间复杂程度等于O(N)
//	//查一次可以去掉一行或一列,key:该位置是这一行的最高/低，这一列的最低/最高，不能相同：右上角和左下角都可以
//	int k = 7;
//	//找到返回1，找不到返回0
//	int x = 3;//行
//	int y = 3;//列
//	//int ret = find_num(arr, 3, 3, k);//找k值,传值
//	int ret=find_num(arr, &x, &y, k);//找k值，传址,通过指针传进去行列信息，带回来坐标信息
//	//1.传入参数
//	//2.待会结果
//	if (ret == 1)
//	{
//		printf("找到了\n");
//		printf("下标是：%d %d\n", x, y);
//	}
//	else
//	{
//		printf("找不到\n");
//	}
//	return 0;
//}
//0(N):如果这个数组有N个元素，解决问题时，最坏情况下，要遍历这个数组N个元素，此时时间复杂度就是O(N)
//N - 查找的次数最坏的情况下是N次，或2N、3N次，都可以，但必须要跟N有关系，时间复杂度都叫O(N)
//N*N次 或N*(N-1) -- O(N^2)
//而小于O(N)即最坏情况下也不能遍历整个数组
//即遍历法不满足要求
//O(1)的意思：数组有N个元素，但不管N是几，最多只会遍历这个数组3次、5次、8次，和N没有关系，是个常数次
//O(1)的效率已经非常高了，时间复杂度非常好

//题目主要为数组、字符串、数据结构
//字符串左旋：实现一个函数，可以左旋字符串中的k个字符
//ABCD->BCDA  ABCD->CDAB
#include<assert.h>
//void reverse(char* left, char* right)//起始地址，结束地址
//{
//	assert(left);
//	assert(right);
//	while (left < right)
//	{
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//
//void string_left_rotate(char* str, int k)
//{
//	//法一：
//	//A B C D E F 一个个挪
//	//int i = 0;
//	//int n = strlen(str);//知道有多少个元素
//	//for (i = 0; i < k; i++)
//	//{
//	//	//每次左旋一个字符
//	//	//1.提取第一个出来
//	//	char tmp = *str;
//	//	//2.把后面的n-1个字符往前挪
//	//	int j = 0;
//	//	for (j = 0; j < n - 1; j++)
//	//	{
//	//		*(str + j) = *(str + j + 1);
//	//	}
//	//	//3.tmp放最后
//	//	*(str + n - 1) = tmp;
//	//}
//	
//	//法二：三步翻转法
//	// A B C D E F 先左边逆序吗，再右边逆序，即B A F E D C，再整体逆序C D E F A B
//	//即只要写一个逆序函数调用三次就可以了
//	//不过如果k太大，会调用野指针，会报错
//	int n = strlen(str);
//	reverse(str,str+k-1);//左
//	reverse(str+k,str+n-1);//右
//	reverse(str,str+n-1);//整体
//}
//
//int main()
//{
//	char arr[10] = "ABCDEF";//数组内容可以随便改
//	//char* p = "ABCDEF";//常量字符串是没办法改的,不能写这种代码
//	//int k = 10;//旋转10次的结果和旋转4次是一样的，因为旋转6次就回到原位了，再来4次就10次了
//	int k = 4;
//
//	string_left_rotate(arr,k);
//	printf("%s\n", arr);
//	return 0;
//}
//右旋同理：只不过是把右边第一个字符备份，前面的往后挪而已

//字符串旋转结果：写函数判断一个字符串是否为另一字符串旋转之后的字符串
//eg:s1=abcd和s2acbd 不是，返回0
   //s1=aabcd和s2=bcdaa返回1
#include<string.h>
int is_string_rotate(char* str1, char* str2)
{
	//如果一个个去试，左旋一个、左旋两个去判断，用法一就可以
	//法一：穷举
	//int i = 0;
	//int n = strlen(str1);//知道有多少个元素
	//for (i = 0; i < n; i++)
	//{
	//	//每次左旋一个字符
	//	//1.提取第一个出来
	//	char tmp = *str1;
	//	//2.把后面的n-1个字符往前挪
	//	int j = 0;
	//	for (j = 0; j < n - 1; j++)
	//	{
	//		*(str1 + j) = *(str1 + j + 1);
	//	}
	//	//3.tmp放最后
	//	*(str1 + n - 1) = tmp;
	//	if (strcmp(str1, str2) == 0)
	//	{
	//		return 1;
	//	}
	//}
	//return 0;

	//法二：取巧，更好，有库函数，方便很多
	//1.str1字符串后面追加一个str1，AABCDAABD
	if (strlen(str1) != strlen(str2))//长度不一样，肯定不是
	{
		return 0;
	}
	int len = strlen(str1);
	strncat(str1, str1,len);//字符串追加
	//2.判断str2是否为str1的字符串：能否在str1里找到str2
	char*ret=strstr(str1,str2);//用于判断一个字符串是否是零一字符串的子字符串，判断str2是不是str1的子字符串
	//当发现str2时str1的子字符串，会返回这个字符串所在的位置，即返回一个地址，找不到会返回一个空指针
	//if (ret == NULL)
	//{
	//	return 0;
	//}
	//else
	//{
	//	return 1;
	//}
	//可以简化：
	return ret != NULL;
	//但也有漏洞，如AABCDAABCD和BCD，BCD时子字符串，但不是旋转得来-->先判断长度
}
int main()
{
	//char arr1[] = "AABCD";
	//取巧：AABCDAABCD这个字符串包含了arr1所有旋转的可能性
	//用arr2在AABCDAABCD里找，找到了就是由它旋来的
	char arr1[20] = "AABCD";
	char arr2[] = "BCDAA";//判断是否由s1旋转的来，可以旋s2试试，看看和s1相不相等
	int ret=is_string_rotate(arr1, arr2);
	if (ret == 1)
	{
		printf("yes\n");
	}
	else
	{
		printf("no\n");
	}
	return 0;
}

int main()
{
	//char arr[10] = "hello";
	//填10会报错，因为追加之后就是5+5，还有一个结束符\0，共11个字符，这个数组只有10个字节空间不够，报错，至少11个
	char arr[11] = "hello";
	//strcat(arr, "bit");//当数组空间足够大时，strcat函数可以追加字符串
	//strcat(arr, arr);//不能追加自己，会出错
	strncat(arr, arr, 5);//加个n就可以追加自己了
	printf("%s\n", arr);
	return 0;
}

//指针梳理：
//一级指针
//二级指针
//
//整型指针、字符指针...
//
//数组指针
//函数指针 - 回调函数
//
//数组：
//一维数组
//二维数组
//
//整型数组、字符数组..
//
//指针数组
//
//指针运算：
//解引用
//指针+-整数
//指针-指针
//指针的关系运算

//cpp - cplusplus ->c++