#define _CRT_SECURE_NO_WARNINGS 1
//将一个四位数反向输出
#include<stdio.h>
//int main()
//{
//	int n = 0;//更可控，不是随机值
//	//输入
//	scanf("%d",&n);//1234
//	//输出
//	while (n)
//	{
//		printf("%d", n % 10);
//		n = n / 10;
//	}
//	return 0;
//}
//实现字母的大小写转换(多组输入)
//int main()
//{
//	//只能处理一个字符
//	int ch = 0;
//	while ((ch = getchar())!=EOF)//若读取失败，则返回EOF
//	{
//		//putchar(ch + 32);
//		printf("%c\n", ch + 32);//printf可以设置换行
//		getchar();//清理\n
//	}
	//ctrl+Z-->EOF-->程序停止	
	//return 0;
	//int ch = getchar();
	//ch = ch + 32;
	//putchar(ch);
	//return 0;
//}//回车也是字符
//输入缓冲区 要瞧回车，才能放入缓冲区加\n,getchar只会拿走输入，不会拿走\n,到下次循环，才会拿走\n,而\n的ascii码值+32产生的字符为*
//正确做法--不希望有\n-->直接等待新输入-->销毁回车
//scanf不读空格，putchar读
int main()
{
	char password[20] = { 0 };
	printf("请输入密码:>");
	scanf("%s", password);
	printf("请确认（Y/N）:>");
	getchar();//处理缓冲区剩下的\n
	//-->多行输入的易错点
	int ch = getchar();//去缓冲区拿走了\n,不等新输入了-->先处理\n
	if (ch == 'Y')
	{
		printf("确认成功\n");

	}
	else
	{
		printf("放弃确认\n");
	}
	return 0;
}