#define _CRT_SECURE_NO_WARNINGS 1
#include"contact.h"

//void InitContact(Contact* pc)
//{
//	pc->sz = 0;
//	memset(pc->data, 0, sizeof(pc->data));//sizeof(数组名)求得是整个数组的大小
//}
//动态版本-初始化
void InitContact(Contact* pc)
{
	pc->data = (PeoInfo*)malloc(DEFAULT_SZ *sizeof(PeoInfo));
	if (pc->data == NULL)
	{
		perror("IniContact");
		return;
	}
	pc->sz = 0;//初始化后默认为0
	pc->capacity = DEFAULT_SZ;
}

void DestroyContact(Contact* pc)
{
	free(pc->data);
	pc->data = NULL;
	pc->sz = 0;//清空有关内存的信息
	pc->capacity = 0;
}

void menu()
{
	printf("******************************\n");
	printf("***** 1.add     2.del    *****\n");
	printf("***** 3.search  4.modify *****\n");
	printf("***** 5.sort    6.print  *****\n");
	printf("***** 0.exit             *****\n");
	printf("******************************\n");
}

//void AddContact(Contact* pc)
//{
//	if (pc->sz == MAX)
//	{
//		printf("通讯录已满，无法添加\n");
//		return;
//	}
//	printf("请输入名字:>\n");
//	scanf("%s", pc->data[pc->sz].name);
//	printf("请输入年龄:>\n");
//	scanf("%d", &(pc->data[pc->sz].age));
//	printf("请输入性别:>\n");
//	scanf("%s", pc->data[pc->sz].sex);
//	printf("请输入电话:>\n");
//	scanf("%s", pc->data[pc->sz].tele);
//	printf("请输入地址:>\n");
//	scanf("%s", pc->data[pc->sz].addr);
//
//	pc->sz++;
//	printf("增加成功\n");
//}
//动态版本的增加联系人
void AddContact(Contact* pc)
{

	if (pc->sz == pc->capacity)
	{
		//考虑增容
		PeoInfo*ptr=(PeoInfo*)realloc(pc->data, (pc->capacity + INC_SZ) * sizeof(PeoInfo));
		if (ptr != NULL)
		{
			pc->data = ptr;
			pc->capacity += INC_SZ;
			printf("增容成功\n");
		}
		else
		{
			perror("AddContact");
			printf("增加联系人失败");
			return;
		}
	}
	printf("请输入名字:>\n");
	scanf("%s", pc->data[pc->sz].name);//地址以数组的形式使用arr[i]等价于*(arr+i)
	printf("请输入年龄:>\n");
	scanf("%d", &(pc->data[pc->sz].age));
	printf("请输入性别:>\n");
	scanf("%s", pc->data[pc->sz].sex);
	printf("请输入电话:>\n");
	scanf("%s", pc->data[pc->sz].tele);
	printf("请输入地址:>\n");
	scanf("%s", pc->data[pc->sz].addr);

	pc->sz++;
	printf("增加成功\n");
}
//还可以收缩空间，当空间中的信息不足空间的百分之多少时，收缩掉一点空间，还是用realloc

static int FindByName(Contact* pc, char name[])
{
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		if(strcmp(pc->data[i].name,name)==0)
		return i;
	}
	return -1;
}

void DelContact(Contact* pc)
{
	char name[MAX_NAME] = { 0 };
	if (pc->sz == 0)
	{
		printf("通讯录为空，无需删除\n");
		return;
	}
	printf("请输入要删除人的名字:>");
	scanf("%s", name);
	int pos=FindByName(pc, name);
	if (pos == -1)
	{
		printf("要删除的人不存在\n");
		return;
	}
	int i = 0;
	for (i = pos; i < pc->sz-1; i++)
	{
		pc->data[i] = pc->data[i + 1];
	}
	pc->sz--;
	printf("删除成功\n");
}

void SearchContact(Contact* pc)
{
	char name[MAX_NAME] = { 0 };
	printf("请输入要查找人的名字:>");
	scanf("%s", name);
	int pos = FindByName(pc, name);
	if (pos == -1)
	{
		printf("要查找的人不存在\n");
		return;
	}
	else
	{
		printf("%-20s\t%-5d\t%-5s\t%-12s\t%-20s\n",
			pc->data[pos].name,
			pc->data[pos].age,
			pc->data[pos].sex,
			pc->data[pos].tele,
			pc->data[pos].addr);
	}

}

void ModifyContact(Contact* pc)
{
	char name[MAX_NAME] = { 0 };
	printf("请输入要修改人的名字:>");
	scanf("%s", name);
	int pos = FindByName(pc, name);
	if (pos == -1)
	{
		printf("要修改的人不存在\n");
		return;
	}
	else
	{
		printf("请输入名字:>\n");
		scanf("%s", pc->data[pos].name);
		printf("请输入年龄:>\n");
		scanf("%d", &(pc->data[pos].age));
		printf("请输入性别:>\n");
		scanf("%s", pc->data[pos].sex);
		printf("请输入电话:>\n");
		scanf("%s", pc->data[pos].tele);
		printf("请输入地址:>\n");
		scanf("%s", pc->data[pos].addr);
		printf("修改成功\n");
	}
}

void PrintContact(Contact* pc)
{
	int i = 0;
	printf("%-20s\t%-5s\t%-5s\t%-12s\t%-20s\n", "名字", "年龄", "性别", "电话", "地址");
	for (i = 0; i < pc->sz; i++)
	{
		printf("%-20s\t%-5d\t%-5s\t%-12s\t%-20s\n",
			pc->data[i].name,
			pc->data[i].age,
			pc->data[i].sex,
			pc->data[i].tele,
			pc->data[i].addr);

	}
}
