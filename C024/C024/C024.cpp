#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//打印1000到2000之间的闰年
//int main()
//{
//	int count = 0;
//	int y = 0;
//	for (y = 1000; y <= 2000; y++)
//	{
//		//判断y是不是闰年
//		//1.被4整除，不能被100整除
//		//2.能被400整除
//		if (y % 4 == 0)
//		{
//			if (y % 100 != 0)
//			{
//				printf("%d ", y);
//				count++;
//			}
//		}
//		if (y % 400 == 0)
//		{
//			printf("%d ", y);
//			count++;
//		}
//	}
//	printf("\ncount=%d\n", count);
//	return 0;
//}
//int main()
//{
//	int count = 0;
//	int y = 0;
//	for (y = 1000; y <= 2000; y++)
//	{
//		if (((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0))
//		{
//			printf("%d ", y);
//			count++;
//		}
//	}
//	printf("count=%d", count);
//	return 0;
//}
//打印100-200的素数
//素数就是质数--只能被1和它本身整除

//int main()
//{
//	int i = 0;
//	for (i = 100; i < 201; i++)
//	{
//		//判断i是否为素数
//		//2->i-1之间去除i，看能不能整除
//		int j = 0;
//		for (j = 2; j < i; j++)
//		{
//			if (i % j == 0)
//			{
//				break;
//			}
//		}
//		if (i == j)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}
//另法
#include<math.h>
//int main()
//{
//	int count = 0;
//	int i = 0;
//	//或
//	//设m=a*b
//	//a和b中一定有一个数字<=开平方m的
//	//即16=2*8=4*4；<=4
//	//sqrt()--开平方函数
//	for (i = 100; i < 201; i++)
//	{
//		
//		int j = 0;
//		int flag = 1;//假设i就是素数
//		//for (j = 2; j < i; j++)//全部除，有点繁琐
//		for (j = 2; j < sqrt(i+1); j++)//只需算2--sqrt（101）试除的数字变少了
//		{
//			if (i % j == 0)
//			{
//				flag = 0;//--实质上时做标记
//				break;
//			}
//		}
//		if (flag==1)
//		{
//			count++;
//			printf("%d ", i);
//			
//		}
//	}
//	printf("%d ", count);
//	return 0;
//}
//还能优化--偶数不可能是素数，因为都是素数-->试除又从源头少了一半--其实j也可以少一半，不可能是偶数
//int main()
//{
//	int count = 0;
//	int i = 0;
//	
//	for (i = 100; i < 201; i+=2)
//	{
//
//		int j = 0;
//		int flag = 1;
//		for (j = 2; j < sqrt(i + 1); j++)
//		{
//			if (i % j == 0)
//			{
//				flag = 0;
//				break;
//			}
//		}
//		if (flag == 1)
//		{
//			count++;
//			printf("%d ", i);
//		}
//	}
//	printf("%d ", count);
//	return 0;
//}
//goto语句
//int main()
//{
//flag:
//	printf("hehe\n");
//	printf("haha\n");
//
//	goto flag;
//	return 0;
//}
//goto其实没必要用，滥用导致死循环
//关机程序--只要运行起来，电脑就在1分钟内关机，如果输入：我是猪，就取消关机
#include<string.h>
#include<stdlib.h>
int main()
{
	//关机，c语言的关机，在command命令提示窗口，有关机的小命令shutdown-->shutdown -s -t 60   -s代表设置关机，-t表设置时间关机，60表设置六十秒关机，回车一敲，倒计时关机开始
	//输入shutdown -a  注销取消关机
	//C语言提供了一个函数：system()--执行系统命令
	char input[20] = { 0 };//字符数组的定义，预留了20个位置，存放输入的信息
	system("shutdown -s -t 60");//system头文件为stdlib.h
//again:	
//	printf("请注意，你的电脑将在1分钟内关机，如果输入:我是猪，就取消关机\n");
//	scanf("%s", input);//数组名本就是地址，不需要取地址了&
//	//if (input == "我是猪")//两个字符串，是不能使用==的，3和5可以，字符串不行-->要用strcmp()全程string compare--字符串比较
//	if (strcmp(input,"我是猪") == 0)//相同会返回0,strcmp头文件为string.h
//	{
//		system("shutdown -a");
//	}
//	else
//	{
//		goto again;//理论上讲，可以不用
//	}
	while (1)
	{
		printf("请注意，你的电脑将在1分钟内关机，如果输入:我是猪，就取消关机\n");
		scanf("%s", input);
		if (strcmp(input, "我是猪") == 0)
		{
			system("shutdown -a");
			break;
		}
	}
	return 0;
}
//Release
//搜如何把一个可执行程序添加到服务里去-->有一些命令可以做到
//服务里，属性里，自动为程序开始即启动，手动为双击才启动
//把程序添加到同学电脑，自动-->像中毒
//苹果电脑不行
//goto语句可以不用，但某些情形可以用，可以用于终止程序在某些深度嵌套的循环中，如跳出两层、三层多次循环
//for的三层循环要三个break，而goto就很方便-->可以跳到下面去
//goto语句只能在一个函数范围内跳转，不能跳转跨函数
//void test()
//{
//flag:
//	printf("test");
//}
//int main()
//{
//	goto flag;//识别不了flag，跨函数hi不行的
//	return 0;
//}








