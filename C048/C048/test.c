#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//输入一个字符，构成一个三角形金字塔
int main()
{
	//输入
	char n = 0;//字符也可以也可以用ASCII，不加单引号也行
	scanf("%c", &n);
	//输出
	int i = 0;
	for (i = 0; i < 5; i++)
	{
		//每次进入打一行
		//先打印空格，在打印字符
		int j = 0;
		for (j = 0; j <5-1-i ; j++)
		{
			printf(" ");
		}
		//再打印
		for (j = 0; j <= i; j++)
		{
			printf("%c ", n);
		}
		printf("\n");
	}
	return 0;
}
//1:14:21