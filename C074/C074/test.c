#define _CRT_SECURE_NO_WARNINGS 1
//回顾：
#include<stdio.h>//perror头文件也是这个
#include<errno.h>
#include<string.h>//strerror的头文件
//int main()
//{
//	//fopen打开文件失败时会返回NULL,成功会返回有效的指针
//	FILE*pf = fopen("test.txt", "r");//读文件
//	if (pf == NULL)
//	{
//		//printf("%s\n", strerror(errno));//strerror会把错误码对应的字符串的首元素地址传过来
//		perror("fopen");//多打印了个fopen: 才到错误信息，那个参数就是自定义要打出来的信息
//		//perror会把错误码对应的错误信息并且打印在自定义信息加:+空格
//		//功能1：首先把错误码转换为错误信息 2：打印错误信息（包含自定义信息）
//		return 1;
//	}
//	fclose(pf);//关闭文件
//	pf = NULL;
//	return 0;
//}
//与strerror对标的函数：perror 打印错误信息的
//void perror(const char* str);直接上手就打印，不需要printf，strerror只是把错误码转换成错误信息，打不打印不管

//字符分类函数：操作字符的
//iscntrl:判断是不是控制字符
//isspacs:判断是否空白字符
//isdigit:是否数字字符
#include<ctype.h>
//int main()
//{
//	//char ch = '#';
//	//char ch = '2';
//	char ch = 'a';
//
//	//int ret=isdigit(ch);//参数是传过来一个ASCII码值，是数字字符返回非0的值，如果不是数字字符，返回0
//
//	//int ret=islower(ch);//是，返回非0，不是，返回0
//	////其他也是
//	//printf("%d\n", ret);
//
//	return 0;
//}

//isxdigt:是否十六进制数字，包括所以十进制数字，小写字母a-f,大写字母A-F
//islower:小写字母a-z
//isupper:大写字母A-Z
//isalpha:字母a-z或A-Z
//isalnum:字母或数字，a-z，A-Z,0-9
//ispunct:标点符号，任意不属于数字或者字母的图形文字
//isgaph:任何图形字符
//isprint:任何可打印字符，包括图形字符和空白字符

//字符转换函数:
//tolower转小写，toupper转大写
//int main()
//{
//	char arr[20] = { 0 };
//	scanf("%s", arr);
//	int i = 0;
//	while (arr[i] != '\0')
//	{
//		if (isupper(arr[i]))
//		{
//			arr[i]=tolower(arr[i]);//转化成小写，tolower返回值是int,ASCII	
//		}
//		printf("%c ", arr[i]);
//		i++;
//	}
//	return 0;
//}

//内存函数:memcpy memmove memmcmp memset
//已掌握知识是有缺陷的
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 0 };
//	//把arr1的前五个元素拷贝到arr2去，让arr2前五个元素放1，2，3，4，5
//	//strcpy()是拷贝字符串的，参数是char*strcpy(char*dest,const char*src),并且遇到\0就停止了，在小端存储里1的存储是01 00 00 00，2是02 00 00 00，如果用strcpy读完01，第二个字节00就拷贝不动了
//	//-->内存函数只关注内存块，不关注内存中的数据是什么
//	return 0;
//}

//memcpy内存拷贝函数
//void* memcpy(void* destination, const void* source, size_t num);//目的空间，源头数据空间，拷贝的空间大小（数据量的大小）
//为什么是void*？无具体类型指针，什么样的数据都可以放进去，第三个参数提供拷贝的数据范围
#include<assert.h>
void* my_memcpy(void*dest,const void*src,size_t num)//不知道程序员拷贝什么数据,num表示拷贝多少个字节数据
{
	void* ret = dest;
	assert(dest && src);
	while (num--)//拷贝n次，后置减减先用再减
	{
		*(char*)dest = *(char*)src;//逐个字节拷过去
		dest=(char*)dest+1;
		src=(char*)src + 1;
		//最靠谱写法
		//void*类型不能++,char*可以
		//*(char*)dest++ = *(char*) src++;//不行，后置++搞不定，强制类型转换之后针对的是dest、src，等到++时强制类型转换已经过去了，是临时性的，等++时已经变回void*了，不能++了
		//前置加加也不合适，要拷贝的
		//操作符的优先级是相邻操作符才有用，如果不相邻，优先级已经没用了
		//()前置类型优先级转换低于后缀自增，即++时强制类型转换还没有产生效果
		//单目运算从右向左也能解释

		//*((char*)dest)++ = *((char*)src)++;//这样倒是可以的
		//这样也不越界，虽然会加到后面，但是没有操作，不算越界，不报错
	}
	return ret;
}

void* my_memmove(void* dest, void* src, size_t num)
{
	//既然从前往后拷贝会重叠，那就试试从后往前呗
	//把源数据从后向前拷贝

	//当我要把34567拷贝到12345,从后向前烤又不行，必须从前向后拷贝了

	//什么时候从前向后、从后向前？
	//1 2 3 4 5 6 7 8 9 10 数组随着下标增长，地址由低到高变化
	//P142图例解释非常好-->
	void* ret = dest;
	assert(dest && src);
	if (dest < src)
	{
		//从前向后拷
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
		
	}
	else
	{
		//从后向前
		while (num--)
		{
			//num已经-1了
			*((char*)dest + num) = *((char*)src + num);

		}
		//在这个情况下dest>src+5时，时会越界的
	}
	return ret;
}
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 0 };
//
//
//	//memcpy(arr2, arr1, 20);//拷贝20个字节空间
//	//my_memcpy(arr2, arr1, 20);
//	//能不能把arr1的12345放到34567的空间中呢
//	//源头从1开始，目的地从3开始
//	//my_memcpy(arr1 + 2, arr1, 20);//1 2 1 2 1 2 1 8 9 10
//	//当源头和目的地重叠：345，拷贝会出问题1拷贝到3，2拷贝到4，到3的位置时3已经变成1了，只能把1拷贝到5的位置，以此类推，画图好懂
//	//p141
//	//-->memcpy应该拷贝不重叠的内存
//	//重叠交给另一函数memmove,可以处理内存重叠的情况
//	
//	//void* memmove(void* destination, const void* source, size_t num);
//	//memmove(arr1+2,arr1,20);
//	//怎么模拟实现memmove
//	//my_memmove(arr1, arr1 + 2, 20);
//	memcpy(arr1, arr1+2, 20);//其实库里的memcpy也可以达到这个效果
//	//c语言中规定，memcpy - 只要实现了不重叠拷贝就可以了，而vs中的实现既可以拷贝不重叠，也可以拷贝重叠内存
//	//相当于c语言认为你不必这样，而vs超常发挥了，而我们写的my_memcpy是符合c语言规范的
//	//既vs库里的memcpy是按照memcpy实现的
//	//但不能保证所有平台都这样
//	return 0;
//}
//mem - memory - 内存
//cpy - copy


//memcmp - 内存比较
//int memcmp(const void* ptr1, const void* ptr2, size_t num);两块内存地址，num表示比较多少个字节
//int main()
//{
//	float arr1[] = { 1.0,2.0,3.0,4.0 };
//	float arr2[] = { 1.0,3.0 };
//	//float4个字节
//	//int ret = memcmp(arr1, arr2, 4);
//	int ret=memcmp(arr1, arr2, 8);
//	//memcmp和strcmp思路一样，相等返回0，str1>str2返回正数，str1<str2返回负数
//	printf("%d\n", ret);
//	return 0;
//}

//memset - 内存设置函数
int main()
{
	//void* memset(void* ptr, int value, size_t num);
	int arr[10] = { 0 };//有块内存
	//把arr前20个字节全部设置为1
	memset(arr, 1, 20);//以字节为单位设置内存的
	//1在内存是01 00 00 00
	//现在是   01 01 01 01
	//字符数组用memset倒也可以哦
	return 0;
}