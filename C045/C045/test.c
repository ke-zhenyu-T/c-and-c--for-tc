#define _CRT_SECURE_NO_WARNINGS 1
//二维数组的创建和初始化
//int main()
//{
	//创建
	//int arr[3][4];//数字里每个元素的类型  数组名  三行四列
	//char ch[3][10];
	//初始化-->创建的同时给赋值
	//int arr[3][4] = { 1,2,3,4,5,6,7,8,9,10,11,12 };//一个一个往后放，打横放
	//int arr[3][4] = { 1,2,3,4,5,6,7 };//多的补0，若为字符数组补0相当于补\0;
	//int arr[3][4] = {(1,2),(3,4),(5,6)};逗号表达式，表达结果为逗号右边的数
	//int arr[3][4] = { {1,2 }, { 3,4 }, { 5,6 } };//把每行分别当做一维数组，在里面也用大括号，多的位置依旧补0
//	int arr[][4] = { {1,2 }, { 3,4 }, { 5,6 } };//行可以省略，列不可以，里面包含三个括号表明是有三行的，即可以根据初始化的情况确定，但每行几个元素是不可以省略的
//	return 0;
//}
//二维数组的使用

//二维数组：第一行号为0，列号也从0开始
#include<stdio.h>
//int main()
//{
//	/*int arr[3][4] = { {1,2 }, { 3,4 }, { 5,6 } };
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 4; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}*/
//	//二维数组在数组中的存储-->布局情况
//	int arr[3][4] = { {1,2 }, { 3,4 }, { 5,6 } };
//	int i = 0;
//	int j = 0;
//	int* p = &arr[0][0];//整型指针
//	for (i = 0; i < 12; i++)
//	{
//		printf("%d ", *p);
//		p++;
//	}
//	/*for (i = 0; i < 3; i++)
//	{*/
//		//for (j = 0; j < 4; j++)
//		//{
//		//	printf("&arr[%d][%d] = %p\n", i, j, &arr[i][j]);//相邻都差4
//		//	//-->二维数组在内存中也是连续存放，每一行内部是连续的，而换行也是连续的！
//		//	//用途有：1.行可省略，列不可，列知道，自然知道怎么放，反正类似线性连续；2.只要拿到第一个元素地址，所有元素都能获得
//		//}
//	//}
//	/*	printf("\n");*/
//	int arr[3][4] = { {1,2 }, { 3,4 }, { 5,6 } };//也可将三行分别看作三个一维数组：数组名分别为：arr[0]、arr[1]、arr[2]
//	return 0;
//}
 

//数组作为函数参数