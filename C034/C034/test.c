#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//有序数组中的二分法
//int binary_search(int a[],int k)//数组接受，接收key，接受sz，形参和实参名字可同可不同
//{	//传过去的是首元素的地址，不是数组，根本就不需要大小，可以不写
//	int sz = sizeof(a) / sizeof(a[0]);//在函数算看看-->调试出sz为1不是10
//	//此时a为指针则sizeof求出为4，4除第一个元素的大小，4/4=1-->sz=1
//	int left = 0;
//	int right = sz - 1;
//	while (left <= right)
//	{
//		
//		int mid = (left + right) / 2;
//		if (a[mid] > k)
//		{
//			right = mid - 1;
//		}
//		else if (a[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else
//		{
//			return mid;
//		}
//	}
//	return -1;//找不到时
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int key = 7;
//	//找到了就返回找到的位置的下标，找不到返回-1,为什么不返回0--0也是下标
//	//int sz = sizeof(arr) / sizeof(arr[0]);//数组的总大小/单个大小==元素个数
//	int ret =binary_search(arr,key);//二进制查找-->二分查找
//	//数组名就能代表数组，写数组名即可，不需方块[],arr[]什么都不是，错误的语法形式，只有定义是可以的，形参的a[]只是为了说明这是个数组
//	//为什么非要传sz
//	//数组arr传参，实际传递的不是数组的本身，仅仅传过去数组首元素的地址，不是整个数组-->即本质上传过去了个指针-->应为 int binary_search(int* a,int k)
//	if (-1 == ret)
//	{
//		printf("找不到");
//	}
//	else
//	{
//		printf("找到了，下标是：%d\n", ret);
//	}
//	return 0;
//}
// 即在函数中sizeof(arr)为指针大小，main函数里是数组大小占用空间
//-->注意，如果函数内部需要参数部分传过来某个元素个数，一定要在外面求好再传过来，想在函数内部求，做不到

//正确答案：

//int binary_search(int a[], int k, int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left <= right)
//	{
//
//		int mid = (left + right) / 2;
//		if (a[mid] > k)
//		{
//			right = mid - 1;
//		}
//		else if (a[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else
//		{
//			return mid;
//		}
//	}
//	return -1;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int key = 7;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int ret = binary_search(arr, key,sz);
//	if (-1 == ret)
//	{
//		printf("找不到");
//	}
//	else
//	{
//		printf("找到了，下标是：%d\n", ret);
//	}
//	return 0;
//}
//写函数，每调用一次，使num+1
//void Add(int* p)
//{
//	(*p)++;
//}
//int main()
//{
//	int num = 0;
//	Add(&num);
//	printf("%d\n", num);//1
//
//	Add(&num);
//	printf("%d\n", num);//2
//
//	Add(&num);
//	printf("%d\n", num);//3
//
//	return 0;
//}
//想在函数内部改变变量--传址调用，和二分法也一样，传地址