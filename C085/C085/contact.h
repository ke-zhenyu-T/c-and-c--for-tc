#pragma once

#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#define MAX_NAME 20
#define MAX_SEX 10
#define MAX_TELE 12
#define MAX_ADDR 30
#define MAX 1000

#define DEFAULT_SZ 3//默认
#define INC_SZ 2//增量

typedef struct PeoInfo
{
	char name[MAX_NAME];
	char sex[MAX_SEX];
	int age;
	char tele[MAX_TELE];
	char addr[MAX_ADDR];
}PeoInfo;

//通讯录-静态版本
//typedef struct Contact
//{
//	PeoInfo data[MAX];
//	int sz;
//}Contact;

//动态版本
typedef struct Contact
{
	PeoInfo* data;//指向动态申请的空间，用来存放联系人的信息
	int sz;//记录当前通讯录中有效信息个数
	int capacity;//记录当前通讯录最大容量
}Contact;


void InitContact(Contact* pc);

void menu();

enum Option
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SORT,
	PRINT
};

void AddContact(Contact* pc);

void DelContact(Contact* pc);

void SearchContact(Contact* pc);

void ModifyContact(Contact* pc);

void PrintContact(Contact* pc);

void DestroyContact(Contact* pc);

//保存通讯录信息到文件
void SaveContact(Contact* pc);

//加载文件内容到通讯录
void LoadContact(Contact* pc);

//检测增容问题
void CheckCapacity(Contact* pc);