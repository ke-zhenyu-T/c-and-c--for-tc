#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include"contact.h"
//版本3：文件版本
//当通讯录退出时，把信息写到文件，当通讯录初始化时，加载文件的信息到通讯录

int main()
{
	int input = 0;
	Contact con;

	//给data申请一块连续的空间在堆区上，sz改为0，capacity初始化为当前最大容量
	InitContact(&con);
	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case ADD:
			AddContact(&con);
			break;
		case DEL://不涉及扩大内存，不用改
			DelContact(&con);
			break;
		case SEARCH://不涉及个数变化，不用改
			SearchContact(&con);
			break;
		case MODIFY:
			ModifyContact(&con);
			break;
		case PRINT:
			PrintContact(&con);
			break;
		case EXIT://要改，空间是动态开辟的，要动态回收
			//保存信息到文件
			SaveContact(&con);
			//退出前销毁通讯录
			DestroyContact(&con);
			printf("退出通讯录\n");
			break;
		default:
			printf("输入错误，重新输入\n");
			break;
		}
	} while (input);
	return 0;
}