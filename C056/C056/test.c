#define _CRT_SECURE_NO_WARNINGS 1
#include<string.h>
#include<stdio.h>
//int main()
//{
//	char str[] = "hello bit";
//	//int a = 0;
//	//scanf("%d",&a);
//	//[hello bit\0]
//	printf("%d %d\n", sizeof(str), strlen(str));//空格也算
//	//10 9
//	//strlen - 函数-求字符串长度的，找\0之前出现的字符个数
//	//sizeof - 操作符-计算变量、类型所占内存大小，单位是字节
//	char acX[] = "abcdefg";//a b c d e f g \0
//	char acY[] = { 'a','b','c','d','e','f','g'};//a b c e d f g
//	//不等价，acX长点多点
//	printf("%c", acX[6]);
//	return 0;
//}//随着下标有小变大，地址也由小变大

//void init(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		arr[i] = 0;
//	}
//}
//void print(int* arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//void reverse(int arr[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left < right)
//	{
//		int tmp = arr[left];//tmp,临时变量
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz=sizeof(arr)/sizeof(arr[0]);
//	print(arr, sz);
//	reverse(arr, sz);//对arr数组逆序 10，9，8，7，6，5，4，3，2，1
//	print(arr, sz);
//	init(arr,sz);//初始化为0
//	print(arr, sz);//打印出来
//	return 0;
//}

//将数组A中的内容和数组B中的内容进行交换
int main()
{
	int arr1[] = { 1,3,5,7,9 };
	int arr2[] = { 2,4,6,8,10 };
	int sz = sizeof(arr1) / sizeof(arr1[0]);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		int tmp = arr1[i];
		arr1[i] = arr2[i];
		arr2[i] = tmp;
	}
	return 0;
}