#define _CRT_SECURE_NO_WARNINGS 1
#include<string.h>
#include<stdio.h>
//函数
//是什么？f(x)=2*x+1;
//c语言的函数就是子程序--维基百科（台湾说法）
//是一个大型程序中的部分代码，有输入输出，有多条语句组成
//c语言函数的分类：1.库函数；2.自定义函数
//1.库函数分类:
//IO函数--输入输出函数printf,scanf,getchar,putchar
//字符串操作函数：strcmp,strlen
//字符操作函数:toupper
//内存操作函数：memcpy,memcmp,memset
//时间/日期函数:time
//数学函数:sqrt开平方,pow求次方
//strcpy
//int main()
//{
//	char arr1[20] = { 0 };
//	char arr2[] = "hello bit";
//	strcpy(arr1, arr2);//把arr2指向的字符串拷贝至arr1的位置或空间--空格和\0都会被拷贝过去的
//	printf("%s",arr1);//打印arr1这个字符串 %s -以字符串的格式打印
//
//	return 0;
//}
//debug是调试版本，release是测试版本
//memset<--memory-记忆-内存
//memse-内存设置
//int main()
//{
//	char arr[] = "hello bit";//要把hello换成x-->hello是前5个
//	memset(arr, 'x', 5);
//	printf("%s\n", arr);
//	return 0;
//}
//库函数有限，自定义函数更重要，自定义函数的使用很重要
