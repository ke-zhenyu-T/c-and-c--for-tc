﻿#define _CRT_SECURE_NO_WARNINGS 1
//qsort函数：quick sort快速排序
//冒泡排序：9 8 7 6 5 4 3 2 1 0 ,要升序，
//冒泡：相邻比较，一个个送上去,一趟冒泡排序解决一组数字里的一个数字到他的对应位置上
#include<stdio.h>
#include<stdlib.h>

//void bubble_sort(int arr[],int sz)
//{
//	int i = 0;
//	//冒泡排序的趟数
//	for (i = 0; i < sz - 1; i++)
//	{
//		//一趟冒泡排序
//		int j = 0;
//		for (j = 0; j < sz-1-i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				//交换
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
void print_arr(int arr[],int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}
//int main()
//{
//	//把arr排成升序
//	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);//元素个数	print_arr(arr,sz);
//	print_arr(arr, sz);
//	bubble_sort(arr,sz);//冒泡排序，是自己写的
//	print_arr(arr, sz);
//	return 0;
//}
//qsort(),是c语言自带的库函数，可以进行排序，用的是快速排序的方法
//用冒泡排序模拟实现qsort

//qsort()什么类型的数据都可以排序，整型、字符串、结构体等数据都可以
//而我们自己写的那个函数却挑数据类型的，qsort可排任意类型

//void qsort(void* base, //void*,无具体类型的指针，就像个垃圾桶，什么都能存
//			size_t num, 
//			size_t size,
//			int (*compar)(const void*, const void*)
//			);//共4个参数，第4个参数是函数指针

//第一个参数base：存放的是待排序数据中第一个对象的地址
//第二个参数num：待排序数据元素的个数
//第三个参数size：待排序数据中一个元素的所占空间，单位是字节（只知道首元素地址和元素的个数，没办法确定每次访问多少字节，因为并不知道地址的类型，整型访问4个字节，字符只访问一个字节，但有了每个元素所占空间，就🆗了）
//第四个参数compar：用来比较待排序数据中的两个元素的函数（数据不一样，比较方法也不一样），返回整型，大于0则第一个元素大于第二个元素，0表示等于，小于0表示小于
//int (*compar)(const void*e1, const void*e2):e1放的是比较元素的一个元素的地址，e2是另一个元素的地址，怎么传是compar函数的事情先不管，void*什么地址都接收
//即第四个参数是函数指针，这个函数需要我们使用者自己来实现

#include<string.h>
int compar_int(const void* e1, const void* e2)//函数比较方法
{
	//*e1 *e2不能直接解引用，void*是无类型的，不知道访问几个字节，不知道怎么解引用
	//return *(int*)e1 - *(int*)e2;//强制类型转换成整型地址，已知是整型了，自然转就可以了
	//降序：
	return *(int*)e2 - *(int*)e1;
}

//void test1()//测试整型的函数
//{
//	int arr[] = { 1,3,5,7,9,2,4,6,8,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//排序
//	qsort(arr, sz, sizeof(arr[0]), compar_int);//第4个参数是比较函数的地址，比较函数的返回类型和参数类型要和qsort的函数指针的参数要相同
//	//打印
//	print_arr(arr, sz);
//}
struct Stu//共24个字节
{
	char name[20];//20个字节
	int age;//4个字节
};
int sort_by_age(const void*e1,const void*e2)
{
	return ((struct Stu*)e1)->age - ((struct Stu*)e2)->age;//转化成结构体类型的指针
	//qsort要求第一个和第二个比较，第一个大返回的是正数
	//降序：
	return ((struct Stu*)e2)->age - ((struct Stu*)e1)->age;
}
int sort_by_name(const void* e1, const void* e2)
{
	//名字是字符串，用strcmp比较
	return strcmp(((struct Stu*)e1)->name, ((struct Stu*)e2)->name);
	//strcmp的返回值和qsort是一样的，第一个大于第二个返回大于0，等于返回0，小于返回小于0
	//两个字符串比较是用对应位置字符的ASCII码值进行比较的，与长度无关，多出来的位置和\0比较
}
//void test2()
//{
//	//使用qsort函数排序结构体数据
//	struct Stu s[] = { {"zhangsan",30},{"lisi",34},{"wangwu",20}};//结构体数组,怎么排序呢？
//	//按照年龄排序
//	int sz = sizeof(s) / sizeof(s[0]);
//	//qsort(s, sz, sizeof(s[0]),sort_by_age);
//	//按照名字来排序
//	qsort(s, sz, sizeof(s[0]), sort_by_name);//比较字符串首字母
//}
//
//int main()
//{
//	test1();
//	//test2();//排序结构体数据
//	return 0;
//}


//假设要写一个冒泡排序函数来排序字符串
//bubble sort str()//字符串比较要用strcmp，数据不一样，比较方法也不一样
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//
//			//if (arr[j] > arr[j + 1])//字符串用strcmp进行比较
//			//{
//			//	int tmp = arr[j];
//			//	arr[j] = arr[j + 1];
//			//	arr[j + 1] = tmp;
//			}
//
//		}
//	}
//
//}

//那qsort如何实现降序？
//升序，qsort第一个大于第二个就交换
//那降序，第一个大于第二个，就不交换
//即将e1、e2调换

//模仿qsort实现一个冒泡排序的通用算法：可排任何类型数据

void Swap(char* buf1, char* buf2, int width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tmp = *buf1;
		*buf1 = *buf2;
		*buf2 = tmp;
		buf1++;
		buf2++;
	}
}

void bubble_sort(void* base,
				int sz,
				int width,
				int(*cmp)(const void*e1,const void*e2))
{
	int i = 0;
	//趟数
	for (i = 0; i < sz - 1; i++)
	{
		//一趟的排序
		int j = 0;
		for (j = 0; j < sz - 1 - i; j++)
		{
			//两个元素的比较
			//若是整型，比较arr[j]和arr[j+1]
			if (cmp((char*)base+j*width,(char*)base+(j+1)*width)>0)//回调函数，回调了
			{
				//交换
				Swap((char*)base + j * width, (char*)base + (j + 1) * width,width);//交换
			}
		}
	}
}
void test3()//测试整型的函数
{
	int arr[] = { 1,3,5,7,9,2,4,6,8,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	//排序
	bubble_sort(arr, sz, sizeof(arr[0]), compar_int);//第4个参数是比较函数的地址，比较函数的返回类型和参数类型要和qsort的函数指针的参数要相同
	//打印
	print_arr(arr, sz);
}

void test4()//结构体排序
{
	//使用qsort函数排序结构体数据
	struct Stu s[] = { {"zhangsan",30},{"lisi",34},{"wangwu",20}};//结构体数组,怎么排序呢？
	//按照年龄排序
	int sz = sizeof(s) / sizeof(s[0]);
	//bubble_sort(s, sz, sizeof(s[0]),sort_by_age);
	//按照名字来排序
	bubble_sort(s, sz, sizeof(s[0]), sort_by_name);//比较字符串首字母
}

int main()
{
	//test3();
	test4();
	return 0;
}

int main()
{
	int a = 10;
	char ch = 'w';
	void* p = &a;//无具体类型,可存放任意类型指针
	p = &ch;
	//*p;//但不能解引用操作，因为不知道访问几个字节
	//p++;//也不行，不知道+1会跳过几个字节，不知道步长
	//必须强制转换才能解引用或计算
	//void*就是垃圾桶，什么都能扔进去
	return 0;
}