int main()
{
	int a = 10000;
	FILE* pf = fopen("text.txt","wb");//wb-以二进制形式写进去
	if (pf == NULL)
	{
		perror("fopen");
		return;
	}
	//写文件
	fwrite(&a, sizeof(int), 1, pf);
	//关文件
	fclose(pf);
	pf = NULL;
	return 0;
}
