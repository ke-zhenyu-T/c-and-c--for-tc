#define _CRT_SECURE_NO_WARNINGS 1
//P167
//文件操作：
//为什么使用文件：保留数据，程序退出后仍能保留数据
//文件：磁盘上的就是
//文件有两种：1.程序文件 2.数据文件
//程序文件包括：源程序文件(.c)，目标文件(.obj)，可执行程序(.exe)
#include<stdio.h>
//int main()
//{
//
//	printf("hehe");
//	return 0;
//}
//文件名包含3部分：文件路径+文件名主干+文件后缀
//C:\code\test.txt

//P167
//文件的打开和关闭：
//文件指针：文件类型的指针
//文件信息区与文件强绑定、关联，维护文件的相关信息：文件名、文件状态、文件当前的位置等
//文件信息区其实是个结构体变量，这个结构体的类型是叫FILE的结构
//一般通过一个FILE的指针来维护这个FILE结构的变量


//通过文件指针变量能找到与它关联的文件
//FILE* pf;
//FILE* fopen(const char* filename, const char* mode);返回文件信息区的地址
//即当用fopen打开文件时，会主动创建一个文件信息区对文件进行操作，并返回文件信息去的起始地址

//文件的打开和关闭：读写前打开，使用结束后关闭
//FILE* fopen(const char* filename, const char* mode);//参数：文件名，文件的打开方式
//打开方式有："r" - 要求文件必须在，不在打不开，会返回空指针
//"w" - 只写，打开一个空文件来写，如果文件里有内容，内容会一切被销毁，变成空文件再写
//"a" - 打开，在文件末尾去写，文件追加

//int main()
//{
//	//FILE* pf=fopen("test.dat","w");//打开文件，返回FILE*的指针
//	//如果用写的形式打开，如果没有这个文件，会创建这个文件出来的
//	//FILE* pf = fopen("test.dat", "r");
//	 //FILE* pf = fopen("D:\code\c-and-c--for-tc\C083\test.dat", "r");\可能会把一些字母转化为字符，认为路径是字符->多加个\，希望\不是\	
//	 FILE* pf = fopen("D:\\code\\c-and-c--for-tc\\C083\\test.dat", "r");//+\是为了让路径不转为转义字符
//	//用只读形式打开，没有这个文件，会出错
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//写文件
//
//	//关闭文件
//	//不关文件：一个进程或程序能打开的文件数量是有限的，文件是种资源，不关闭到最后就打不开文件了
//	//int fclose(FILE * stream);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//文件的顺序读写：
//int main()
//{
//	FILE* pf = fopen("test.dat", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//写文件
//	//fputc('b', pf);
//	//fputc('i', pf);
//	//fputc('t', pf);
//	//一个个写进去，按顺序的
//	//屏蔽后又没了
//	//即只要以w打开，即使有内容也会清空的
//	//也能写进标准输出流里
//	//流：高度抽象的概念 - 统一读写规则，接收数据并写到不同的设备（屏幕，硬盘，u盘，光盘，网络，软盘）
//	//-->可以向文件流里写数据
//	//C语言程序，只要运行起来，就默认打开了3个流：
//	//stdin, -标准输入流 - 键盘
//	//stdout - 标准输出流 - 屏幕
//	//stderr - 标准错误流 - 屏幕
//	//三个流都是FILE*的指针，管理的都是流
//
//	
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//文件的随机读写
//int fputc(int c, FILE* stream);//可以把数据显示到屏幕上去
//第一个参数是要写的那个字符的ASCII码值，第二个参数是指针指向的文件结构

//int main()
//{
//	fputc('b', stdout);
//	fputc('i', stdout);
//	fputc('t', stdout);
//	return 0;
//}

//fputc使用所有输出流
//fgetc使用所有输入流

//用fgetc从文件里读取：

//int main()
//{
//	FILE* pf = fopen("test.dat", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	int ret=fgetc(pf);//fgetc如果读取正常，会返回读取字符的ASCII码值，如果读取失败或文件结束时，会返回EOF
//	//int fgetc(FILE * stream);
//	//EOF是-1，char无法接收，只能用整形去接收
//	printf("%c\n", ret);
//	ret = fgetc(pf);
//	printf("%c\n", ret);
//	ret = fgetc(pf);
//	printf("%c\n", ret);
//	ret = fgetc(pf);
//	printf("%c\n", ret);
//	ret = fgetc(pf);
//	printf("%c\n", ret);
//	ret = fgetc(pf);
//	printf("%c\n", ret);
//	ret = fgetc(pf);
//	printf("%c\n", ret);
//	ret = fgetc(pf);
//	printf("%c\n", ret);
//	ret = fgetc(pf);
//	printf("%c\n", ret);
//	ret = fgetc(pf);
//	printf("%c\n", ret);
//	//读取太多，没有东西读取时，也会返回EOF
//
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//fgetc如果不是针对文件，也可以在键盘上读取
//用fgetc从标准输入流里读取
//int main()
//{
//	int ret = fgetc(stdin);
//	printf("%c\n", ret);
//	ret = fgetc(stdin);
//	printf("%c\n", ret);
//	ret = fgetc(stdin);
//	printf("%c\n", ret);
//	ret = fgetc(stdin);
//	printf("%c\n", ret);
//	ret = fgetc(stdin);
//	printf("%c\n", ret);
//	ret = fgetc(stdin);
//	printf("%c\n", ret);
//	ret = fgetc(stdin);
//	printf("%c\n", ret);
//	ret = fgetc(stdin);
//	printf("%c\n", ret);
//	ret = fgetc(stdin);
//	printf("%c\n", ret);
//
//	//只能读三个，因为读了三次
//
//	return 0;
//}
//fgets,fputs读取一行、写一行
//int main()
//{
//	char arr[10] = "xxxxxx ";
//	//FILE* pf = fopen("test.dat", "w");//写一行
//	FILE* pf = fopen("test.dat", "r");//读一行
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//写文件 - 按行写
//	//fputs("abcdef\n", pf);
//	//fputs("qwerty\n", pf);
//	//读文件 - 读一行
//	fgets(arr,4,pf);
//	printf("%s\n", arr);
//	fgets(arr, 4, pf);//读4个其实是读3个，要留个给\0
//	//读完一行内容为止，不会从下一行开始读
//	printf("%s\n", arr);
//
//	//char* fgets(char* string, int n, FILE * stream);n是读取的最大个数，最多能读取多少个字符
//	//如果n是100，最后读取的是99个字符，因为要留一个给\0
//	//没换行
//	//int fputs(const char* string, FILE * stream);写字符串进流里
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//fgets适用于所有输入流 fputs适用于所有输出流

//格式化输入函数、格式化输出函数
//struct S
//{
//	char arr[10];
//	int num;
//	float sc;
//};
//int main()
//{
//	struct S s = { "abcdef",10,5.5f };
//	//格式化的数据进行写文件
//	FILE* pf=fopen("test.dat", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//	}
//	//写文件
//	//int printf( const char *format [, argument]... );
//	//int fprintf(FILE * stream, const char* format[, argument]...);...是可变的的意思，表可能有多个参数 - 即参数的个数是可以发生变化的，是可变的
//	//格式化写入
//	fprintf(pf," % s % d % f", s.arr, s.num, s.sc);
//
//	//还能还原结构体
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//int main()
//{
//	//struct S s = { "abcdef",10,5.5f };
//	struct S s = { 0 };
//	//FILE* pf = fopen("test.dat", "w");
//	FILE* pf = fopen("test.dat", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//	}
//	fprintf(pf, " % s % d % f", s.arr, s.num, s.sc);
//
//	//还能读出来还原结构体
//	//int scanf( const char *format [,argument]... );
//	//int fscanf(FILE * stream, const char* format[, argument]...);
//	//参数也是多了个流而已
//	//当流的位置填stdin，fscanf等价于scanf
//	fscanf(pf,"%s %d %f",s.arr,&(s.num),&(s.sc));//格式化输入函数
//	//当fscanf(stdio,)//就是从屏幕中读取，就是printf
//	//打印
//	//printf("%s %d %f\n", s.arr, s.num, s.sc);
//	fprintf(stdout, "%s %d %f\n", s.arr, s.num, s.sc);
//	//fprintf和printf也等价，当流变成stdout
//	//这里的格式化，是以固定的形式操作：读取、写入
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//二进制输入和输出：以二进制的方式写文件，读文件
//二进制的读写
//struct S
//{
//	char arr[10];
//	int num;
//	float sc;
//};

//int main()
//{
//	//struct S s = { "abcde",10,5.5f };
//	struct S s = { 0 };
//	//二进制的形式写：
//	FILE*pf=fopen("test.dat","r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//写文件
//	//size_t fwrite(const void* buffer, size_t size, size_t count, FILE * stream);参数：指针，指向的数据要被写；一个元素有多大，最多几个元素，流
//	//size_t fread(void* buffer, size_t size, size_t count, FILE * stream);函数原型和上面这个没有上面区别
//
//	//fwrite(&s,sizeof(struct S),1,pf);//会有出错的地方,二进制写进去的看不懂，能懂abccde是因为字符串以二进制形式写进去的内容以文本形式写是一样的，但整数和浮点型、字符以二进制写入，看不懂了
//	//我们读不懂，也许fread可以
//	//fread可以从流里带回数据，然后到buffer这
//	//读取文件：fread
//	//fread(&s, sizeof(struct S), 1, pf);//也可以写多个文件
//	fread(&s, sizeof(struct S), 10, pf);//也可以写多个文件，这里最多写10个，但最终写几个文件取决于s里的数据
//	printf("%s %d %f\n", s.arr, s.num, s.sc);
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//-->改造通讯录

//P170
//对比一组函数：
//scanf和fscanf和sscanf
//scanf:输入性函数，指定形式输入，是针对标准输入（键盘）的格式化（以固定格式）的输入语句
//int scanf(const char* format[, argument]...);
//fscanf：既针对标准输入，也可针对输入流，即针对所有输入流的格式化的输入语句
//sscanf：
//int sscanf(const char* buffer, const char* format[, argument] ...);从一个字符串里读到一个格式化的数据
//比起scanf多了个buffer - 从字符串buffer里读出一个格式化的数据，剩下的参数使用和scanf一样

//printf和fprintf和sprintf
//printf：针对标准输出的格式化输出语句 - stdout
//int printf( const char *format [, argument]... );
//fprintf：针对所有输出流的格式化输出语句 - stdout/文件
//sprintf：
//int sprintf(char* buffer, const char* format[, argument] ...);把格式化的数据写到字符串里
//就比printf多了个参数而已

//struct S
//{
//	char arr[10];
//	int age;
//	float f;
//};
//int main()
//{
//	struct S s = { "hello",20,5.5f };
//
//	struct S tmp = { 0 };
//	//把结构体的数据转换成字符串
//	//sprintf把一个格式化的数据转化成字符串
//	char buf[100] = { 0 };
//	sprintf(buf, "%s %d %f", s.arr, s.age, s.f);
//
//	printf("%s\n", buf);
//
//	//从buf字符串中还原出一个结构体数据
//	sscanf(buf,"%s %d %f",tmp.arr,&(tmp.age),&(tmp.f));//从buf里读，按照%s %d %f格式去读
//	printf("%s %d %f\n", tmp.arr, tmp.age, tmp.f);
//	//两个打印虽然一样，但打印地形式不一样，前面的是以字符串形式打印，后面的是以字符串，整形，浮点型分别打印
//	//sscanf：从一个字符串中读取格式化数据
//
//	//buf不一定要结构体，只转一个整形也ok的
//	//算是种转化吧
//	return 0;
//}

//文件的随机读写（挑位置，想写哪写哪,想读哪读哪）
//fseek根据文件指针的位置和偏移量来定义文件指针
//int fseek(FILE* stream, long offset, int origin);//让文件指针到我们想要的位置上去
//参数：针对哪个流，偏移量，起始位置
//起始位置的选项有三个：
//SEEK_CUR 表当前文件指针的位置，从当前位置开始偏移
//SEEK_END 从文件末尾开始偏移
//SEEK_SET 文件起始位置开始偏移

//ftell返回文件指针相对于起始位置的偏移量
//int main()
//{
//	FILE* pf = fopen("test2.dat", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return;
//	}
//	//读取文件
//	int ch = fgetc(pf);
//	printf("%c\n", ch);//a
//
//	//调整文件指针
//	//fseek(pf, -1, SEEK_CUR);//aab
//	//fseek(pf, 2, SEEK_CUR);//ade
//	fseek(pf, -2, SEEK_END);//从f的后面向前偏移两个，即e，打印ef
//	//超出界限会出错误，什么也不打印
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//	int ret=ftell(pf);
//	//long ftell(FILE * stream);
//	printf("%d\n", ret);
//
//	//让文件指针回到起始位置
//	rewind(pf);
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//二进制文件和文本文件（看得懂和看不懂）
//数据在内存中以二进制形式存储，如果不加转换输出到外存，称二进制文件
//若在外存中以ASCII码值形式存储，则需要在存储前转换，以ASCII码值存储的文件解释文本文件

//整数10000，若以ASCII码值形式输出到磁盘，则磁盘中占5个字节（每个字符一个字节），而以二进制形式输出，只占4个字节
//00110001 00110000 00110000 00110000 00110000 - 10000的ASCII码
//00000000 00000000 00000000 00010000 - 二进制
//int main()
//{
//	int a = 10000;
//	FILE* pf = fopen("text.txt","wb");//wb-以二进制形式写进去
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return;
//	}
//	//写文件
//	fwrite(&a, sizeof(int), 1, pf);
//	//关文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//10000的二进制：
//0000 0000 0000 0000 0010 0111 0001 0000
//转16进制: 0 0 0 0 2 7 1 0-->00 00 27 10-->小端：10 27 00 00

//文件读取结束的判定：
//被错误使用的feof

//feof是应用于当文件读取结束时，已经读取结束了，来判断是文件读取失败结束的、还是遇到文件结尾结束的，用来判断读取结束的原因

//fgetc在读取结束时，会返回EOF，正常读取时，返回读取到的字符的ASCII码值

//fgets在读取结束时，会返回空指针NULL，正常读取时返回
//char* fgets(char* string, int n, FILE* stream);从流里读最多n的字符到字符串里，返回字符串的地址
//如果读取失败返回空指针
//正常读取时，返回存放字符串的空间的起始地址

//fread函数在读取时，返回的是实际读取到的完整元素的个数
//如果发现读取到的完整元素的个数小于 指定的元素个数，这就是最后一次读取了

//判断结束的原因，是feof

//写代码吧test.txt拷贝一份，生成test2.txt文件
//读写文件问题
//读一个写一个
//int main()
//{
//	FILE* pfread = fopen("text.txt", "r");
//	if (pfread == NULL)
//	{
//		return 1;
//	}
//	FILE* pfwrite = fopen("text2.txt", "w");
//	if (pfwrite == NULL)//新文件打开失败时，第一个文件肯定成功了
//	{
//		fclose(pfread);
//		pfread = NULL;
//		return 0;
//	}
//	//文件打开成功
//	//读写文件
//	int ch = 0;
//	while ((ch = fgetc(pfread)) != EOF)//都打开成功了，就开始读吧
//	{
//		//写文件
//		fputc(ch, pfwrite);
//	}
//	if (feof(pfread))
//	{
//		printf("遇到文件结束标志，文件正常结束\n");
//	}
//	else if (ferror(pfread))
//	{
//		printf("文件读取失败结束\n");
//	}
//	//关闭文件
//	fclose(pfread);
//	pfread = NULL;
//	fclose(pfwrite);
//	pfwrite = NULL;
//
//	return 0;
//}
//ferror
//int ferror(FILE* stream);返回int
//若返回非0，表因为文件读取失败而停止
//feof如果是遇到文件结束返回EOF，如果遇到文件末尾而结束，返回一个非0数字
//如果返回真，证明确实读到文件末尾了
//feof返回真是正常，ferror返回真是异常

//文件缓冲区：系统自动在内存中为程序中每一个正在使用的文件开辟一块”文件缓冲区”
//从内存想磁盘输出数据会先送到内存中的缓冲区，装满缓冲区后才一起送到磁盘上，如果从磁盘向计算机读入数据，则从磁盘文件中读取数据输入到内存缓冲区（充满缓冲区），然后在从缓冲区组个地将数据送到程序数据区（程序变量等）。缓冲区大小根据c编译系统决定
//导图在P170
//缓冲区是为了减少IO，相当于攒问题一起问

#include<Windows.h>
int main()
{
	FILE* pf = fopen("test3.txt", "w");
	fputs("abcdef", pf);//先将代码放在输出缓冲区

	printf("睡眠10秒-已经写数据了，打开test3.txt文件，发现文件没有内容\n");
	Sleep(10000);
	printf("刷新缓冲区\n");
	fflush(pf);//刷新缓冲区时，才会将输出缓冲区地数据写到文件（磁盘）
	//注：fflush在高版本地VS上不能使用了,但2019还是OK的
	printf("再睡眠10秒-此时，再次打开test3.txt文件，文件有内容了\n");
	Sleep(10000);

	fclose(pf);
	//注：fclose在关闭文件时，也会刷新缓冲区
	pf = NULL;

	return 0;
}