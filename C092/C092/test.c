#define _CRT_SECURE_NO_WARNINGS 1
//printf()函数的返回值,打印几个字符就返回几，打印错误会返回负数
#include<stdio.h>
//返回打印在标准输出流上的个数
//int main()
//{
//	int ret=printf("Hello world!");
//	printf("\n");
//	printf("%d", ret);
//	return 0;
//}

//简洁写法：
//int main()
//{
//	return 0;
//	printf("\n%d\n", printf("Hello world!"));
//}


int main()
{

	printf("%d", printf("%d", printf("%d",43)));
	return 0;
}