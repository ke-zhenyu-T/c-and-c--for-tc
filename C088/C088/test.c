#define _CRT_SECURE_NO_WARNINGS 1
//32位下，编译选项位4字节对齐，sizeof(A)和sizeof(B)？
#include<stdio.h>
#pragma pack(4)
//struct A
//{
//	int a;
//	short b;
//	int c;
//	char d;
//}; 
//struct B
//{
//	int a;
//	short b;
//	char c;
//	int d;
//};
//#pragma pack()//取消默认对齐数
//
//int main()
//{
//	struct A sa = { 0 };
//	struct B sb = { 0 };
//	printf("%d\n", sizeof(sa));
//	printf("%d\n", sizeof(sb));
//}

//#pragma pack(4)
//struct tagTest1
//{
//	short c;
//	char d;
//	long b;
//	long a;
//};//12
//
//struct tagTest2
//{
//	long b;
//	short c;
//	char d;
//	long a;
//}; //12
//
//struct tagTest3
//{
//	short c;
//	long b;
//	char d;
//	long a;
//};//16
//#pragma pack()
//int main()
//{
//	struct tagTest1 stT1;
//	struct tagTest2 stT2;
//	struct tagTest3 stT3;
//	printf("%d %d %d", sizeof(stT1), sizeof(stT2), sizeof(stT3));
//}

//#define A 2
//#define B 3
//#define MAX_SIZE A+B
//struct _Record_Struct
//{
//	unsigned char Env_Alarm_ID : 4;//一个字节，用8个比特位
//	unsigned char Paral : 2;
//	unsigned char state;
//	unsigned char avail : 1;
//}*Env_Alarm_Record;//位段式结构体大小：3个字节
////struct _Record_Struct* pointer = (struct _Record_Struct*)malloc(sizeof(struct _Record_Struct) * MAX_SIZE);
//
//int main()
//{
//	int sz = (sizeof(struct _Record_Struct) * MAX_SIZE);
//	printf("%d\n", sz);
//	return 0;
//}

//int main()
//{
//	unsigned char puc[4];
//	struct tagPIM
//	{
//		unsigned char ucPim1;
//		unsigned char ucData0 : 1;
//		unsigned char ucData1 : 2;
//		unsigned char ucData2 : 3;
//	}*pstPimData;
//	pstPimData = (struct tagPIM*)puc;
//	memset(puc, 0, 4);
//	pstPimData->ucPim1 = 2;
//	pstPimData->ucData0 = 3;
//	pstPimData->ucData1 = 4;
//	pstPimData->ucData2 = 5;
//	//位段式内存从高地址位向低地址位使用空间
//	printf("%02x %02x %02x %02x\n", puc[0], puc[1], puc[2], puc[3]);//02 29 00 00
//	//P180
//	return 0;
//}

//算联合体的大小
//union Un
//{
//	short s[7];//14个字节，7个元素，自增大小是2，默认对齐数是8，对齐数仍是2
//	int n;//4个字节，4 8 -->4
//
//};//16个字节
//int main()
//{
//	printf("%d\n", sizeof(union Un));
//	return 0;
//}

//int main()
//{
//	union
//	{
//		short k;
//		char i[2];
//
//	}*s, a;//联合体成员共用一块空间-->2字节
//	s = &a;
//	s->i[0] = 0x39;
//	s->i[1] = 0x38;
//	printf("%x\n", a.k);//大小端-->小端-->低位数据放在低地址处-->0x38 39
//	return 0;
//}
//枚举类型
//enum ENUM_A
//{
//	X1,//0
//	Y1,//1
//	Z1 = 255,
//	A1,//256
//	B1//257
//};
//int main()
//{
//	enum ENUM_A enumA = Y1;
//	enum ENUM_A enumB = B1;
//	printf("%d %d\n", enumA, enumB);
//	 
//	return 0;
//}

typedef struct
{
	int a;//0-3
	char b;//4
	//5--浪费
	short c;//6-7
	short d;//8-9
	//-->10个字节
	//最大对齐数是4
	//10偏移和11偏移浪费
	//-->12个字节
}AA_t;
int main()
{
	printf("%d\n", sizeof(AA_t));
	return 0;
}