#define _CRT_SECURE_NO_WARNINGS 1
//常见的动态内存开辟的错误：
//1.对NULL指针的解引用操作
//int main()
//{
//	int*p=(int*)malloc(1000000000);
//	//如果没判断
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;//空指针+i也是个错误的指针，是非法访问内存
//	}
//	//-->需要对malloc函数的返回值作判空处理
//	return 0;
//}

//2.对动态开辟空间的越界访问
#include<stdio.h>//NULL是放在stdio文件里的，引了才能用
//int main()
//{
//	int* p = (int*)malloc(10 * sizeof(int));
//	if (p == NULL)
//	{
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 40; i++)
//	{
//		*(p + i) = i;//存在越界访问，只开辟了40个字节，只管理了10个整形的空间，一旦超了，就是越界访问
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

//3.对非动态开辟空间内存使用free释放
//使用free释放非动态开辟的空间
#include<stdlib.h>
//int main()
//{
//	int arr[10] = { 0 };//在栈区上的
//	int* p = arr;
//
//	free(p);//释放了非动态开辟的空间
//	p = NULL;
//	return 0;
//}

//4.用free释放动态内存中的一部分
//要释放就一块释放，释放不了一半的
//int main()
//{
//	int* p = malloc(10 * sizeof(int));
//	if (p == NULL)
//	{
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		*p++ = i;//p变化了
//	}
//	free(p);//p变化了，前面的被漏掉了，没人知道起始空间在哪里
//	//风险1：只释放了一部分
//	//风险2：没人知道起始空间了，有内存泄露的风险
//	return 0;
//}

//5.对同一块动态内存开辟的空间，多次释放
//int main()
//{
//	int* p = (int*)malloc(100);
//	free(p);
//	//free(p);
//	//万一在函数内部释放了，出函数后又释放了，很容易的
//	//解决：释放后设NULL
//	p = NULL;//free传空指针什么事情也不会发生
//	free(p);
//	return 0;
//}

//6.动态开辟的空间忘记释放 - 内存泄露，比较严重
//void test()
//{
//	int* p = (int*)malloc(100);
//	if (p == NULL)
//	{
//		return;
//	}
//	//使用
//}
//int main()
//{
//	test();
//	//...
//	//出了函数之后没人记得那块空间了，没人维护了，起始地址都不知道了，内存就泄露了
//	//想释放都找不到了
//	//类似知道卧底身份的都没了-->可以写博客《C间道》
//	return 0;
//}
//动态开辟的空间有两种回收方式：
//1.主动释放free
//2.程序结束（整个main函数结束，生命周期到了，程序都没了，内存也会还回去的）
//如果电脑程序不停运行，又忘记释放，程序不结束又没法释放，每天都泄露内存，卡挂了，重启程序，又ok
//-->要避免-->malloc和free成对使用

//出现内存泄露，就要调试排查
//笔试题：
#include<string.h>
//char* GetMemory(char* p)//p是临时拷贝的，p里得到了str传的值，p里放的也是NULL
//{
//	p = (char*)malloc(100);//申请了100字节的空间，p此时放的是新空间的地址
//	//解决方法：
//	//改法一
//	return p;
//
//}
//void Test(void)
//{
//	char* str = NULL;
//	str=GetMemory(str);//str是个指针变量，把str本身传过去了，这里是值传递
//	//p是形参变量出了函数就销毁了
//	strcpy(str, "hello world");//str还是原来的str，和新空间没有关系，是NULL，把字符拷贝到空指针所指向的空间，有问题，拷贝失败
//	//且由于p的销毁，动态开辟的空间找不到了，也销毁不了，内存泄露了
//	printf(str);
//	//再释放也释放不了了，p已经销毁，已经内粗泄露了
//
//	//str传给GetMemory函数时时值传递，所以GetMemory函数中的形参p时str的一份临时拷贝，所以在GetMemory内部动态申请空间的地址存在p中，不会影响外边的str，所以当GetMemory函数返回之后，str依然是NULL，所以strcpy会失败
//	//且当GetMemory返回后，形参p销毁，使得动态开辟的100个字节存在内存泄露，无法释放
//	printf(str);//? ->ok的
//	printf("hello,world");//实际上是char*p="hello,world",hello,world一个常量字符串作为一个表达式，产生的结果是h的地址，把h的地址放p里去
//	//所以printf里实际上是h的地址，所以printf打印的是h这个地址指向的字符串
//	//所以printf(str)就是传的字符指针，当然可以
//
//	free(str);
//	str = NULL;
//}
//int main()
//{
//	Test();
//	return 0;
//}
//该法二：传址
//char* GetMemory(char** p)
//{
//	*p = (char*)malloc(100);
//
//}
//void Test(void)
//{
//	char* str = NULL;
//	str = GetMemory(&str);
//	strcpy(str, "hello world");
//	printf(str);
//
//	free(str);
//	str = NULL;
//}
//int main()
//{
//	Test();
//	return 0;
//}

//char* GetMemory(void)
//{
//	char p[] = "hello world";
//	return p;
//	//虽然返回了p，返回了字符串首字符地址，但一旦出了函数，p指向的空间会还给操作系统
//}
//void Test(void)
//{
//	char* str = NULL;
//	str = GetMemory();//str存了地址，但对应的空间还给操作系统了，再打印，有可能已经被覆盖了，变成随机值，非法访问了
//	printf(str);//打印出随机值
//	//这种情况统一叫 - 返回栈空间地址的问题
//	//栈上开辟，出了范围会销毁的
//	//上一题的改法一：是返回对空间的地址，是堆区的空间
//}
//int main()
//{
//	Test();
//	return 0;
//}
//GetMemory函数内部创造的数组是在栈区上创建的，出了函数，p数组的空间就还给了操作系统，返回的地址是没有实际意义的，如果通过返回的地址去访问内存，就是非法访问内存

//Nice校招笔试
//int* f1(void)
//{
//	int x = 10;
//	return (&x);//x是局部变量，出了函数没意义了，销毁了，返回栈空间地址
//}
//
//int* f2(void)
//{
//	int* ptr;
//	*ptr = 10;
//	return ptr;//这个是野指针问题，ptr虽然是个指针变量，但没有初始化，放的是随机值，局部变量不初始化就是随机值，被解引用，10不知道放哪里，不知道访问哪里
//	//野指针问题、
//	//指针要么是空指针，要么明确指向某一空间
//}
//void GetMemory(char** p, int num)
//{
//	*p = (char*)malloc(num);
//	//唯一的问题是没有free
//}
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(&str, 100);
//	strcpy(str, "hellow");
//	printf(str);
//	//只需加上：
//	//free(str);
//	//str = NULL;
//}
//int main()
//{
//	Test();
//	return 0;
//}

//void Test(void)
//{
//	char* str = (char*)malloc(100);
//	strcpy(str, "hello");
//	free(str);
//	//已经释放了，用不了的了
//	if(str!=NULL)
//	{
//		//free是不会让str置空的，str仍然是那一串地址，只不过我们不能访问了而已
//		strcpy(str, "world");//没有权限，不让改，属于非法访问内存
//		//知道银行地址，但不能抢银行
//		printf(str);
//	}
//}
//int main()
//{
//	Test();
//	return 0;
//}
//free之后一定要置空

//P160 - C/C++中程序内存区域划分
//内核空间（用户代码不能读写）
//栈（向下增长） - 栈区
//内存映射段（文件映射、动态库匿名映射）
//堆（向上增长） - 堆区
//数据段（全局数据、静态数据）  - 静态区
//代码段（可执行代码/制度常量）

//栈区：在执行函数时，函数内部变量的单元都可以在栈上创建，函数执行结束时这些存储单元自动被释放。占内存分配运算内置于处理器的指令中，效率发哦，但分配的内容量有限。栈区主要存放运行函数而分配的局部办理、函数参数、返回数据、返回地址等
//堆区：一般由程序员分配释放，若程序员不释放，程序结束时可由操作系统os回收，分配方式类似于链表
//数据段（静态区）：存放全局变量、静态数据，程序结束后由系统释放
//代码段：存放函数体（类成员函数和全局函数）的二进制代码

//typedef int* int_ptr;//类型重定义，把int*起了个别名int_ptr
//则int_ptr就表示整个整型指针类型，是完整的类型
//int_ptr c, d;//所以这里c、d都是整形指针类型

//柔性数组
//int* a, b;//其中a是指针类型，b是整形，因为*给了a，表示a是指针类型，给了a，就没有了，int表示a是指向整形的指针，b是整形
//要表现都是指针，用int*p1,*p2
//C99中存在，大小可变的数组

//struct S
//{
//	int n;
//	int arr[];//数组不指定大小，大小是未知的
//	//此时这个数组称为柔性数组
//};
//struct S
//{
//	int n;
//	int arr[0];
//};//这个形式也是柔性数组，数组大小写成0
//VS2019支持C99
//1.结构中的柔性数组成员前必须至少有一个其他成员
//2.sizeof返回的这种结构大小不包含柔性数组的内存大小
//3.包含柔性数组的结构用malloc函数进行内存的动态分配，并且分配的内存应该大于结构的大小，以适应柔性数组的预期大小

//int main()
//{
//	struct S s = { 0 };//单纯这样创建里面的arr是无法使用的
//
//	期望arr的大小是10个整形	
//	printf("%d\n", sizeof(s));
//
//	struct S*ps=(struct S*)malloc(sizeof(struct S)+10*sizeof(int));//也给arr准备了10个元素大小
//	malloc，体现柔性，柔软，可调
//	ps->n = 10;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		ps->arr[i]=i;
//	}
//	空间不够就增容
//	struct S*ptr=(struct S*)realloc(ps, sizeof(struct S) + 20 * sizeof(int));
//	if (ptr != NULL)
//	{
//		ps = ptr;
//	}
//	free(ps);
//	ps = NULL;
//	return 0;
//}

struct S
{
	int n;
	int* arr;//用这个指针指向的空间动态开辟不就和数组一样了吗
	//arr还没有指向空间-->为arr动态开辟一块空间
};
int main()
{
	struct S* ps = (struct S*)malloc(sizeof(struct S));
	if (ps == NULL)
	{
		return 1;
	}
	ps->n = 10;
	ps->arr = (int*)malloc(10 * sizeof(int));
	if (ps->arr == NULL)
	{
		return 1;
	}

	int i = 0;
	for (i = 0; i < 10; i++)
	{
		ps->arr[i] = i;
	}
	//能达到和之前一样的效果

	//还可以增加
	int*ptr=(int*)realloc(ps->arr, 20 * sizeof(int));
	if (ptr != NULL)
	{
		ps->arr = ptr;
	}

	//回收：
	//先free第二块空间，若先free第一块，第二块都找不到了
	free(ps->arr);
	ps->arr = NULL;
	free(ps);
	ps = NULL;
	return 0;
}
//虽然能达到功能，但仍有不足:
//进行了两次malloc，对应两次free，容易出错，柔性数组只需一次，相对好一点
//两次malloc，会留下不大不小的内存碎片，再次被利用的可能性比较低，增加内存碎片不好
//频繁使用内存块：1.带来内存碎片，2.效率低
// 
//内存池：直接申请好一块大空间，直接用就是了

//柔性数组的意义
//局部性原理
//空间局部性：使用一个地方内存时，接下来大概率使用其周边内存的数据
//柔性数组好：一次开辟一次释放，方便内存释放
//有利于访问速度，减少内存碎片的产生（一定程度上）
