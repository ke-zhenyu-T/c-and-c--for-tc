#define _CRT_SECURE_NO_WARNINGS 1
//sizeof(数组名) - 此时数组名表示整个数组，计算的是整个数组的大小
//&数组名 - 数组名也表整个数组，取出的是整个数组的地址
//除此之外，所有数组名都是数组首元素地址
#include<stdio.h>
//int main()
//{
//	int a[] = { 1,2,3,4 };
//	//printf("%d\n", sizeof(a));//计算整个数组大小 - 16
//	//printf("%d\n", sizeof(a + 0));//数组名不是单独放进sizeof里的，是a+0放进去的，是除此之外的情况，是作为首元素地址的，+0还是首元素地址，是第一个元素的地址，是算地址的大小，地址的大小：4或8
//	//printf("%d\n", sizeof(*a));//不是单独放进，是除此之外的情况，a表首元素地址，先解引用，*a是数组第一个元素，sizeof(*a计算的是第一个元素的大小)
//	//printf("%d\n", sizeof(a+1));//除此之外情况，第二个元素的地址，计算的是地址的大小
//	//printf("%d\n", sizeof(a[1]));//计算第二个元素的大小
//
//	printf("%d\n", sizeof(&a));//取出整个数组的地址，但仍是个地址呀，sizeof计算地址的大小，就是4或8字节
//	printf("%d\n", sizeof(*&a));//取出a数组地址再解引用，取出的是一维数组的地址，解引用找到整个数组，数组大小16个字节
//	//&和*可以理解为抵消，直接算数组大小也正确
//	//&a -- int(*p)[4]= &a;     数组的地址，类型是数组指针
//	printf("%d\n", sizeof(&a+1));//&a取出整个数组的地址，＋1跳过整个数组，下个空间的地址，还是地址，4或8字节
//	printf("%d\n", sizeof(&a[0]));//取出第一个元素的地址，4或8字节
//	printf("%d\n", sizeof(&a[0]+1));//取出第一个元素地址，步长为一个元素，+1第二个元素地址，依然是4或8字节
//	return 0;
//}

//字符数组
//int main()
//{
//	char arr[] = { 'a','b','c','d','e','f' };
//	//printf("%d\n", sizeof(arr));//6 计算整个数组大小
//	//printf("%d\n", sizeof(arr+0));//8 除此之外情况，数组名计算首元素地址，地址大小，字符的地址也是地址
//	//printf("%d\n", sizeof(*arr));//1 除此之外情况，表首元素地址，解引用，找到a，1个字节
//	//printf("%d\n", sizeof(arr[1]));//1 第二个元素大小
//	//printf("%d\n", sizeof(&arr));//8 取出整个一维数组的地址，地址4or8
//	//printf("%d\n", sizeof(&arr+1));//8 &arr取出整个数组地址，+1跳到数组后面的地址
//	//printf("%d\n", sizeof(&arr[0]+1));//8 取出第一个元素的地址，类型int*，＋1跳过一个元素，即第二个元素的地址 4or8字节
//	//&arr+2跳过12个字节的
//	//arr没有\0
//	printf("%d\n", strlen(arr));//没有sizeof，只能是首元素地址，strlen就是给它一个地址，它从该地址开始数字符，数到\0停下来，不知道\0在哪，-->随机值
//	printf("%d\n", strlen(arr + 0));//首元素+0还是首元素，从第一个元素开始数，和上面一样，随机值
//	printf("%d\n", strlen(*arr));//int strlen(const char* str)的函数设计只收地址，*arr传过去的是字符a，ASCII码值为97，传过去的是97，strlen把97当地址，不知道找到哪里去，报错
//	printf("%d\n", strlen(arr[1]));//传过去b，传过去的是98，报错
//	printf("%d\n", strlen(&arr));//取出数组的地址，但传过去，就是首元素的地址了，因为strlen只接受char*，传过去编程char*，认为是首元素的地址，也是随机值，如果要接收数组的地址，就要char(*)[6]
//	printf("%d\n", strlen(&arr + 1));//+1跳过一个数组，从数组后面的地址开始数，不知道是多少，但是随机值-6
//	printf("%d\n", strlen(&arr[0] + 1));//从b开始向后数，和前的随机值比起来少1
//	return 0;
//}

//int main()
//{
//	char arr[] = "abcdef";//多了结束符\0
//	//[a b c d e f \0]7个字符
//	//printf("%d\n", sizeof(arr));//7个元素，每个元素是char
//	//printf("%d\n", sizeof(arr+0));//首元素地址，4or8
//	//printf("%d\n", sizeof(*arr));//1
//	//printf("%d\n", sizeof(arr[1]));//1
//	//printf("%d\n", sizeof(&arr));//4or8，类型是char(*)[7]
//	//printf("%d\n", sizeof(&arr+1));//跳过整个数组，但还是地址，类型还是char(*)[7]
//	//printf("%d\n", sizeof(&arr[0]+1));//第一个元素的地址取出来+1，第二个元素的地址，4or8字节
//
//	printf("%d\n", strlen(arr));//6 没有sizeof和&，数组名就是首元素地址
//	printf("%d\n", strlen(arr + 0));//6
//	//printf("%d\n", strlen(*arr));//报错
//	//printf("%d\n", strlen(arr[1]));//报错
//	printf("%d\n", strlen(&arr));//6，取出的是整个数组的地址，类型是char(*)[7],但strlen接收是强制转化为char*,为首元素地址
//	printf("%d\n", strlen(&arr + 1));//随机值，从元素末尾向后数，不知道，是随机值
//	printf("%d\n", strlen(&arr[0] + 1));//5 从第二个元素开始数
//
//	return 0;
//}

int main()
{
	char* p = "abcdef";
	//a b c d e f \0
	//p变量存放a的地址
	//printf("%d\n", sizeof(p));//字符指针4or8
	//printf("%d\n", sizeof(p + 1));//b的地址，还是地址4or8
	//printf("%d\n", sizeof(*p));//p指向a，*p拿出a，1字节
	//printf("%d\n", sizeof(p[0]));//把字符串当数组访问，p[0]等价与*(p+0)
	//printf("%d\n", sizeof(&p));//p的地址也是地址呀
	//printf("%d\n", sizeof(&p + 1));//跳过一个p，但仍是地址4or8
	//printf("%d\n", sizeof(&p[0] + 1));//第一个元素地址+1变成第二个元素地址

	printf("%d\n", strlen(p));//6 把a的地址交给strlen，向后数
	printf("%d\n", strlen(p+1));//5 从b向后数
	//printf("%d\n", strlen(*p));//err a传的是97，报错
	//printf("%d\n", strlen(p[0]));//err 同上，等同*p
	printf("%d\n", strlen(&p));//取p的地址，二级指针，strlen从二级指针数，数出什么不知道，随机值
	printf("%d\n", strlen(&p + 1));//二级指针再+1，再向后数，随机值，这个随机值和上面的随机值没有任何关系，完全无关，因为p存放着4个字节，如果这4个字节有\0，则strlen一定小于4，p+1后如果半天都遇不到\0,strlen就很长，所以这两个随机值基本没什么关系，因为你不知道里面有什么
	//如地址0x00120048，\0就是0来的
	//&p+1,跨过了一个地址p，4个字节
	//strlen(&p)是从二级指针指向的内容，即从地址p向后找，即找一级指针0x00120048来数
	printf("%d\n", strlen(&p[0] + 1));//b的地址向后数 5

	return 0;
}





//printf("%d\n", sizeof(arr));//
//printf("%d\n", sizeof(arr + 0));//
//printf("%d\n", sizeof(*arr));//
//printf("%d\n", sizeof(arr[1]));//
//printf("%d\n", sizeof(&arr));//
//printf("%d\n", sizeof(&arr + 1));//
//printf("%d\n", sizeof(&arr[0] + 1));//
