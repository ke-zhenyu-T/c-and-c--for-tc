#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//将三个整数从大到小输出
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	//输入
//	scanf("%d%d%d", &a, &b, &c);
//	//调整顺序
//	if (a < b)
//	{
//		int tmp = a;//里面定义，局部变量作用域为局部
//		a = b;
//		b = tmp;
//	}
//	if (a < c)
//	{
//		int tmp = a;
//		a = c;
//		c = tmp;
//	}
//	if (b < c)
//	{
//		int tmp = b;
//		b = c;
//		c = tmp;
//	}
//	//输出--大到小
//	printf("%d %d %d",a,b,c);
//	return 0;
//}
//打印3的倍数
//int main()
//{
//	int i = 0;
	//for (i = 1; i < 100; i++)
//	for (i = 3; i < 100; i+=3)
//	{
//		printf("%d\n", i);
//		//判断i是否为3的倍数
//		/*if (i % 3 == 0)
//		{
//			printf("%d\n", i);
//		}*/
//		//or不判断
//	}
//	return 0;
//}
//给两数，求最大公约数
//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d%d", &m, &n);//24 18
//	最大公约数不会超过18
//	int max = m > n ? n : m;
//	int min = m > n ? m : n;
//	or
//	假设最大公约数就是m和n的较小值
//	int max = 0;
//	if (m > n)
//		max = n;
//	else
//		max = m;
//	while (1)
//	{
//		if (m % max == 0 && n % max == 0)
//		{
//			printf("最大公约数就是:%d\n", max);
//			break;
//		}
//		max--;
//	}
//	while (1)
//	{
//		if (min % m == 0 && 0 == min % n)
//		{
//			printf("最小公倍数是:%d\n", min);
//			break;
//		}
//	}
//
//
//	return 0;
//}
//法二：辗转相除法 设m =24 n =18
//先m%n=6，然后m=18，n=6 m%n=0-->n为最大公约数
int main()
{
	int m = 0;
	int n = 0;
	scanf("%d%d", &m, &n);//24 18若小的在前，多循环一次而已，小的模大的等于小的，然后调转过来了
	int m1 = m;
	int m2 = m;
	int n1 = n;
	int n2 = n;
	int t1 = 0;
	while (t1=m1%n1)
	{
		m1 = n1;
		n1 = t1;
	}
	printf("最大公约数是%d\n", n1);
	//求最小公倍数的另法：=m*n/最大公约数
	int t2 = m > n ? m : n;
	while(t2!=m2*n2/n1)
	{
		t2++;
	}
	printf("最小公倍数是%d\n", t2);
	return 0;
}

// 纯享版
//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d%d", &m, &n);
//	int max = m > n ? n : m;
//	int min = m > n ? m : n;
//	while (1)
//	{
//		if (m % max == 0 && n % max == 0)
//		{
//			printf("最大公约数就是:%d\n", max);
//			break;
//		}
//		max--;
//	}
//	while (1)
//	{
//		if (min % m == 0 && 0 == min % n)
//		{
//			printf("最小公倍数是:%d\n", min);
//			break;
//		}
//		min++;
//	}
//	return 0;
//}