#pragma once


#include<stdio.h>
#include<stdlib.h>
#include<time.h>


//符号的定义
#define ROW 3
#define COL 3



//函数的声明

//初始化棋盘的函数
void InitBoard(char board[ROW][COL], int row, int col);//行可写可不写,列不可省略,传入的数组只是形式上时数组，本质仍是指针，即可数组形式，也可指针形式展示


//打印棋盘的函数
void DisplayBoard(char board[ROW][COL], int row, int col);


//玩家下棋
void PlayerMove(char board[][COL],int row,int col);

//电脑下棋
void ComputerMove(char board[ROW][COL], int row, int col);


//1.玩家赢-- * ；2.电脑赢--#；3.平局--Q；4.游戏继续--C

//判断游戏是否有输赢
char IsWin(char board[ROW][COL], int row, int col);

