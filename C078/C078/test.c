#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//动态内存管理
//栈区（局部变量，函数形参，）（临时用一下的，不需要太大）
//堆区(动态内存开辟)（malloc,free,realloc,callo）
//静态区（数据段）(全局变量、静态变量)

//动态内存开辟
//malloc 开辟一段连续的空间
//void* malloc(size_t size);size大小（开辟多少个字节），返回void*
//如果开辟成功，就会返回该空间地址，如果失败，返回空指针
#include<stdlib.h>
//int main()
//{
//	//假设开辟10个整形的空间(10*sizeof(int))
//	//int arr[10];//栈区
//	//动态内存开辟
//	//void* p = malloc(10 * sizeof(int));
//	//*p;//void*指针不能直接解引用的
//
//	int* p = (int*)malloc(10 * sizeof(int));//默认void*,不能传给int*，会有警告，需要强制类型转化成int*
//	//int* p = (int*)malloc(10000000000 * sizeof(int));
//	//有可能会开辟失败的
//	//malloc返回的是void*，帮你开辟空间，但不知道类型的
//	//开辟0字节是未定义的写法，有毛病的
//	//使用malloc开辟的空间时
//	if (p==NULL)
//	{
//		//printf("malloc error\n");
//		perror("main");//打印出main: xxxxxxx 
//		//perror=printf+strerror
//		return 0;
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p[i]);//可以当作数组用，因为p[i]等价于*(p+i)
//	}
//	//使用完要自己还（自己申请自己换）
//	//回收空间(free)
//	free(p);//空间还回去，但该空间仍然保留原内容，保留空间的地址，但再访问就会非法访问内存
//	//free没办法把p置成空指针，因为free()传过去的是值，如果是&p还有可能，但这里不是
//	//-->用完手动把p置成空指针NULL
//	//使用完一定要拿free函数释放，专门用来动态内存的回收和释放
//	//free函数的参数是开辟的空间的起始地址
//	
//	//如果不是动态开辟的地址，free函数是未定义的
//	//int a = 10;
//	//int* p = &a;
//	//free(p);//这是有问题的，这里的p是指向局部变量的，是指向栈区的，是标准为定义的行为，是错的，函数什么事情也不会做
//
//	//free只是把空间还回去了，但p里依然保存着空间的起始地址-->让p失忆-->强行把p置成NULL
//	
//	//malloc和free一个申请一个释放，必须成对出现
//
//
//	p = NULL;
//	return 0;
//}
//calloc 开辟一个数组，使其初始化为0，也是开辟空间
//void* calloc(size_t num, size_t size);两个参数，元素的个数和每个元素的长度，单位是字节
//malloc和calloc参数不一样，calloc会初始化，其他的用法功能都一样，和free成对
//calloc返回的指针类型和参数里的字节是一样的

//int main()
//{
//	//int* p = (int*)malloc(40);
//
//	int* p = calloc(10, sizeof(int));
//
//	if (p == NULL)//其实不用判断，上亿都开辟得了，何况几十个字节
//	{
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//
//		printf("%d ", *(p + i));
//	}
//	free(p);
//	p = NULL;
//	//-->malloc不会赋初值，打印出来的都是随机值
//	//calloc会对内存初始化，赋0
//
//	return 0;
//}

//realloc重新开辟空间 才能让空间变大/变小，让动态内存管理变灵活
//void* realloc(void* memblock, size_t size);参数是指针指向前面开辟的内存空间的起始地址，要调整谁就是谁，第二个参数是新的大小字节

//int main()
//{
//	int* p = (int*)calloc(1000, sizeof(int));//有些编译器要求强转，有些不要，还是强转一下吧
//
//	if (p == NULL)
//	{
//		perror("main");
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = 5;
//	}
//	//再来10个整形，需要p指向的空间更大，需要20个int的空间
//	//realloc调整空间
//	//realloc(p,20*sizeof(int));//不是增加的大小，是新的大小
//	//返回的依然是指针，指向重新调整后的内存块-->仍然是原来的首地址
//	
//	//那要不要再用p再接收一次新空间地址呢？
//	//realloc在调整空间时，第一种情况：后面空间够，就在后面加空间，返回原空间的首地址即可
//	//第二种情况;后面空间不够，会在堆区内存中重新找一块足够大的放新的空间进行开辟，会把原空间内容拷贝到新空间，同时把原空间销毁、还给系统，此时返回的是新空间的首地址
//	//详细解释P156
//	//所以应该再接收一次，但不是拿p接收（如果要增容，realloc可能找不到空间了调整大小，找不到空间增大了，realloc会返回空指针）
//	//结果p变成NULL，再也找不到空间了，原空间都找不到了，偷鸡不成蚀把米
//	//-->拿个临时指针接收先，然后判断，ok了再用p接收
//	//int* ptr = realloc(p, 20 * sizeof(int));
//	int* ptr =(int*) realloc(p, 999 * sizeof(int));
//	if (ptr != NULL)
//	{
//		p = ptr;//判断没问题了再用p接收，更合适
//	}
//	//aseert只是判断，没有执行的
//
//	free(p);
//	p = NULL;
//	return 0;
//}



//realloc单独使用时也能实现malloc的效果
int main()
{

	int* p = (int*)realloc(NULL, 40);//这里功能类似于malloc，就是直接在堆区开辟40个字节

}
//malloc、calloc开辟空间
//free释放空间
//realloc调整空间
//就可以把通讯录改造成动态增长的版本了