#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAX_NAME 20
#define MAX_SEX 10
#define MAX_TELE 12
#define MAX_ADDR 30
#define DEFAULT_SZ 3
#define INC_SZ 2

typedef struct PeoInfo
{
	char name[MAX_NAME];
	char sex[MAX_SEX];
	int age;
	char tele[MAX_TELE];
	char addr[MAX_ADDR];
}PeoInfo;

typedef struct Contact
{
	PeoInfo* data;//具体联系人的内存地址
	int sz;//已登记数
	int capacity;//容量
}Contact;

void InitContact(Contact* pc);

void menu();

enum Option
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SORT,
	PRINT
};

void AddContact(Contact* pc);

void CheckCapacity(Contact* pc);

void DelContact(Contact* pc);

void SearchContact(Contact* pc);

void ModifyContact(Contact* pc);

void PrintContact(Contact* pc);

void SaveContact(Contact* pc);

void LoadContact(Contact* pc);

void DestroyContact(Contact* pc);