#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//int i;//全局变量，不初始化，默认为0
//int main()
//{
//	i--;
//	//sizeof这个操作符，算出的结果的类型是unsigned int
//	//i有符号，有符号和无符整型比较,会先转化为无符号整型，再将结果比较
//	//-1放到内存里补码是32个1，无符号解读，都是有效位，是一个超大的数，大于4
//	if (i > sizeof(i))//-1>4
//	{
//		printf(">\n"); 
//	}
//	else
//	{
//		printf("<\n");
//	}
//	return 0;
//}

//int main()
//{
//	//由a组成的前n像之和 - 不考虑溢出
//	int a = 0;
//	int n = 0;
//	scanf("%d %d", &a, &n);
//	int i = 0;
//	int sum = 0;
//	int ret = 0;//一项
//	for (i = 0; i < n; i++)
//	{
//		//算出一项
//		ret = ret * 10 + a;
//		sum = sum + ret;
//	}
//	printf("sum = %d\n", sum);
//	return 0;
//}

//写函数打印arr数组内容，不适应数组下标，用指针
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;//其实地址
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int* pend = arr + sz - 1;
//	int i = 0;
//	while (p <= pend)//知道循环多少次就用for循环，不知道就用while循环
//	{
//		printf("%d ", *p);
//		p++;
//	}
//	//for (i = 0; i < sz; i++)
//	//{
//	//	printf("%d ", *(p + i));
//	//}
//
//	
//	return 0;
//}

//打印水仙花数 0~100000,即所以自幂数
#include<math.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 100000; i++)
//	{
//		//判断i是否为自幂数
//		//1.获取i是几位数，计算i的位数
//		//
//		int n = 1;//i的位数,任何数至少是一位数
//		int tmp = i;
//		while (tmp / 10)
//		{
//			n++;//算出n位数
//			tmp = tmp / 10;
//		}
//		//2.计算i的每一位的n次方之和
//		tmp = i;
//		int sum = 0;
//		while (tmp)
//		{
//			//pow是用来求次方数的，求a的b次方pow(a,b)
//			sum += pow(tmp % 10, n);//是个函数，实现次方，算出tmp%10的n次方
//			tmp = tmp / 10;
//		}
//		//3.判断
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//int main()
//{ 
//	unsigned long pulArray[] = { 6,7,8,9,10 };
//	unsigned long* pulPtr;
//	pulPtr = pulArray;//相当于存入了首元素地址，指向的是6
//	*(pulPtr + 3) += 3;//pulPtr+3指向的是9，解引用，拿到9，+=3变成12，元素内容变12
//	printf("%d,%d\n", *pulPtr, *(pulPtr + 3));//6,12,pulPtr没改变过
//	return 0;
//}
//
//struct stu
//{
//	int num;
//	char name[10];
//	int age;
//};
//void fun(struct stu* p)
//{
//	printf("%s\n", (*p).name);
//	return;
//}
//int main()
//{
//	struct stu students[3] = {
//		{9801,"zhang",20},
//		{9802,"wang",19},
//		{9803,"zhao",18}
//	};
//	fun(students + 1);//数组名即首元素地址，+1传过去第二个元素名，即第二个元素的地址
//	return 0;
//}
//int* arr[]写法错误，要写大小的
//*和[]先[]后*，小括号()最优先

//struct S
//{
//	int a;
//	int b;
//};
//int main()
//{
//	struct S a, * p = &a;
//	a.a = 99;
//	printf("%d\n", a.a);//ok
//	printf("%d\n",*p.a );//不行，.的优先级更阿高
//	printf("%d\n", p->a);//ok
//	printf("%d\n", (*p).a);//先解引用相当于先找到了结构体a
//
//
//	return 0;
//}

//写函数，字符串逆序
#include<assert.h>
#include<string.h>
//void reverse(char* str)
//{
//	int len = strlen(str);//求字符串长度,
//	//strlen函数定义：以字符串首元素地址为开始，找到\0为结束
//	char* left = str;
//	char* right = str + len - 1;
//	while (left<right)
//	{
//		//assert(str != NULL);
//		assert(str);//也行
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	char arr[] = "abcdef";//变fedcba
//	//int sz = sizeof(arr) / sizeof(arr[0]);//也ok的,不过sizeof会把\0也算上，不好
//	//char* arr = "abcdef";//不行，"abcedf"是常量字符串，传过去的是常量字符串的地址，解引用也改不了常量的呀
//	reverse(arr);
//	printf("%s\n", arr);
//	return 0;
//}

//打印菱形
//int main()
//{
//	int line = 0;
//	scanf("%d", &line);//7
//	//上下部分逻辑相反，先打印上半部分
//	int i = 0;
//	for (i = 0; i < line; i++)	 
//	{
//		//打印一行
//		//空格
//		//*
//		int j = 0;
//		for (j = 0; j <line-1-i ; j++)
//		{
//			printf(" ");
//		}
//		for (j = 0; j <2*i+1 ; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	//下部分
//	for (i = 0; i < line - 1; i++)
//	{
//		//打印一行，空格
//		int j = 0;
//		for (j = 0; j <=i ; j++)
//		{
//			printf(" ");
//		}
//		for (j = 0; j <2*(line-1-i)-1 ; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

//喝汽水，1瓶汽水1元，2个空瓶换一瓶水，给20元，可以买都是汽水
//39
//int main()
//{
//	int money = 0;
//	int total = 0;
//	scanf("%d", &money);
//	if (money > 0)
//		total = 2 * money - 1;
//
//	//int total = money;//买回来多少瓶
//	//int empty = money;//买回来喝完剩下的空瓶
//	////开始置换
//	//while (empty >= 2)
//	//{
//	//	total+=empty / 2;
//	//	empty = empty / 2 + empty % 2;//喝完的和余下的空瓶
//
//	//}
//	printf("%d\n", total);
//	return 0;
//}
//规律，能喝2*money-1 瓶

