#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//数组作为函数参数
//将数组作为参数传给函数
//冒泡排序-->两两相邻的元素进行比较，并且可能的话需要交换！ -->一趟解决一个数字的位置
//void bubble_sort(int arr[],int sz)//形参arr本质为指针
//{	//确定躺数
//	//计算数组元素个数
//	//sz=1，sizeof(arr)算的是指针大小，在x86平台，地址是32个二进制位，是四个字节，所以指针的大小为4，arr[0]是整形元素，占4个字节，4/4=1
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		//一趟冒泡排序的过程
//		int j = 0;
//		for (j = 0; j <sz-1-i ; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				//交换
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//
//}
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	//排序为升序--冒泡排序--10个数字要9躺，n个数字就是n-1躺
//	/*第1趟：10个数字待排序--9对比较
//		第2趟：9个数字待排序--8对比较
//		第3趟：8个数字待排序--7对比较*/
//
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr,sz);//泡泡,数组传参的时候，传递的是数组首元素的地址
//	//应该求好再传进去的，这里的arr是首元素地址
//
//	return 0;
//}
// 
//效率更高的：若已知有顺序
void bubble_sort(int arr[], int sz)
{	
	int i = 0;
	for (i = 0; i < sz - 1; i++)
	{
		
		int j = 0;
		int flag = 1;
		for (j = 0; j < sz - 1 - i; j++)//每个相邻元素都比较，若一个交换都没有，说明已经有序了，不需要冒泡排序了
		{
			if (arr[j] > arr[j + 1])
			{
				//交换
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
				flag = 0;
			}
		}
		if (flag == 1)
		{
			break;
		}
	}

}
int main()
{
	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr,sz);
	return 0;
}

//数组名是什么
//int main()
//{
//	int arr[10] = { 0 };
//	printf("%p\n", &arr);//1.取出的是数组的地址，因为数组的地址和首元素地址本来就是一样的
//	printf("%p\n", &arr+1);//整个数组地址+1,长了十六进制的28，就是十进制的40
//	printf("%p\n", arr+1);//首元素地址+1，长了4
//	printf("%p\n", &arr[0]);
//	//int sz = sizeof(arr);//40
//	//数组名是首元素地址，但有两意外：1.sizeof(数组名)-->数组名表整个数组-计算的是整个数组的大小，单位是字节
//	//2.&数组名-这里的数组名表示整个数组-取出的是整个数组的地址（sizeof其实是操作符，不是函数）
//	/*printf("%d\n", sz);

//	printf("%p\n", &arr[0]);
//	printf("%p\n", &arr);*///打印相同-->数组名就是数组首元素地址
//	return 0;
//}
//当数组传参时，形参部分，无法计算元素个数
