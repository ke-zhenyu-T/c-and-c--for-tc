#define _CRT_SECURE_NO_WARNINGS 1
//判断字母
//从键盘任意输入一个字符，判断是否字母
#include<stdio.h>
int main()
{
	int ch = 0;//装字符的，字符本质是ASCII码值，当然可以用整形，且，若读取失败，会返回EOF-end of file-->-1-->当然应该整形
	while ((ch = getchar()) != EOF)
	{
		//判断字母
		//@2we!--都是字符，A-->65 a-->97 -->ASCII码值
		if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
		{
			printf("YES\n");
		}
		else
		{
			printf("NO\n");
		}
		//清理掉\n
		getchar();
	}
	return 0;
}
//58min
