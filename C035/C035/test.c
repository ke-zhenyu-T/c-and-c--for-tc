#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//函数的嵌套调用和链式访问
//函数不能嵌套定义，但可以嵌套调用-->我调你，你调他
//void test3()
//{
//	printf("hehe");
//}
//int test2()
//{
//	test3();
//	return 0;//有int，有返回
//}
//int main()
//{
//	test2();
//	return 0;
//}
//int main()
//{
//	//int len = strlen("abc");
//	//printf("%d\n", len);
//	////跳过len，strlen返回值作为printf的参数-->返回值作为参数-->链式反应
//	//printf("%d\n", strlen("abc"));
//	//char arr1[20] = { 0 };
//	//char arr2[] = "bit";
//	////strcpy(arr1, arr2);//把arr2的值拷贝到arr1的空间里，而后返回arr1的地址
//	////printf("%s\n", arr1);
//	//printf("%s\n", strcpy(arr1, arr2));//链式访问
//	printf("%d", printf("%d", printf("%d", 43)));//4321,printf()返回的是打印在屏幕上的字符的个数
//	return 0;
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	//函数声明一下--告知作用，存不存在无关紧要
//	//int Add(int, int);x和y相加就加
//	int c = Add(a, b);
//	printf("%d\n", c);
//	return 0;
//}
//函数的定义--创造作用--也是一种强有力的声明
//int Add(int x, int y)//编译器从前往后扫描，函数最好放前面，若要把函数定义放后面，要声明一下
//{
//	return x + y;
//}
//函数的声明一般放头文件
//写一个计算器
//A - 加法
//B - 减法
//C - 乘法
//D - 除法
//#include"add.h"//相当于把头文件的内容拷贝过来了，也相当于声明了
//而函数定义一般放对应.c文件-->模块最好要对应-->这才是做项目的做法
// 函数声明一般放.h文件,就是引用出来而已
//文件移除不是删除，移除只是没放工程里，还在目录里的
#include"sub.h"
//导入静态库
#pragma comment(lib,"C036.lib")
int main()
{
	int a = 10;
	int b = 20;
	//int c = Add(a, b);
	int c = Sub(a, b);
	printf("%d\n", c);
	return 0;
}