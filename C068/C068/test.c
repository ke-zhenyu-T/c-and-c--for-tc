#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
////一级指针传参：
//void print(int *ptr,int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(ptr + i));
//	}
//}
//void test(char* p)//若接收用一级指针接收，可以传什么参数过来呢
//{
//	;//万一别人弄好了函数，我们要思考怎么用
//
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;//首元素地址,一级指针
//	int sz = sizeof(arr)/sizeof(arr[0]);
//	
//	char ch = 'w';
//	char*p1=&ch
//	test(&ch);//传char的地址
//	test(p1);//指针变量传参，指针变量接收，类型完全匹配，也没问题
//	print(p, sz);//一级指针传参
//
//	return 0;
//}

//二级指针传参
//void test(int **p2)//二级传，二级收，OK
//void test(int **p2)//而当函数参数设计为二级指针，可以接收什么参数呢？
////传二级指针，一级指针变量的地址，存放一级指针的数组 都ok
//{
//	**p2 = 20;
//}
//int main()
//{
//	int a = 10;
//	int *pa=&a;//pa一级指针
//	int** ppa = &pa;//ppa是二级指针，专门用于存放一级指针变量的地址
//	//把二级指针进行传参
//	test(ppa);//二级指针当然可以传
//	test(&pa);//也行，pa是一级指针，取地址是二级指针，所以传一级指针变量的地址也没问题
//	int* arr[10] = { 0 };//存放整型指针的数组，每个元素int*
//	test(arr);//首元素地址，即int*的地址，就是int**
//	printf("%d\n", a);
//	return 0;
//}
//一级指针：
//int* p - 整型指针 - 指向整型的指针  char*pc - 字符指针 - 指向字符的指针  void*pv - 无类型的指针
//二级指针：
//char**p  int **p
//数组指针：指向数组的指针
//int (*p)[4]

//数组：
//一维数组
//二维数组
//指针数组 - 存放指针的数组

//函数指针：指向函数的指针！存放函数地址的指针

//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	char ch = 'w';
//	char* pc = &ch;
//	int arr[10] = { 0 };
//	int (*parr)[10]=&arr;//去除数组的地址
//	//parr是指向数组的指针，存放的是数组的地址
//	
//	//函数指针 - 存放函数地址的指针
//	//&函数名 - 取到的是函数的地址
//	printf("%p\n", &Add);
//	printf("%p\n", Add);//一模一样
//	//数组名 !=&数组名 不等价，数组名取地址取出的是整个数组的地址，而数组名表首元素地址
//	//但函数名==&函数名 完全等价的，函数哪有首元素地址，只是写法形式上不同而已，没有区别的
//	//若
//	int (*pf)(int,int)=&Add;//pf函数指针的变量
//	//首先*表pf是指针，然后(),表达指针指向的是函数，函数的参数类型是int，int，返回类型是int，放前面
//	//此时，pf就是一个函数指针变量，类似数组指针的写法
//	return 0;
//}
//void test(char* str)
//{
//
//}
//int main()
//{
//	 void (*pt)(char*) = &test;//没写void，没写返回类型，会报错
//	return 0;
//}

//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	//那如何调用函数
//	int (*pf)(int, int) = &Add;
//	//int (*pf)(int, int) = Add;//这样也ok 则Add==pf,等价
//	//int ret=(*pf)(3,5);//先解引用，找到那个函数,再用()传参数过去
//
//	//int ret = Add(3, 5);//通过函数名方式去调用，最原始方法
//	//int ret = (*pf)(3, 5);//为什么等价？因为这个*是个摆设，只是方便理解，写多个*都无所谓
//	//int ret = (****pf)(3, 5);也OK的
//	int ret = pf(3, 5);//这种写法和上面的也是等价的
//	
//	//int ret=*pf(3, 5);//就不可以，()优先，即先调用了函数Add，可以调用的，得到8，然后对8进行解引用
//	//只有函数指针解引用的*可以看作摆设，其他的都是有意义的
//
//	printf("%d\n", ret);
//	return 0;
//}
//函数名其实就是地址

//(*(void (*)())0)();//从0开始分析
//0x0012ff48 - int类型,不是地址，不是指针，要变成地址，要先转化类型
//int main()
//{
//	//( void(*p)() )0;//0是int，转化成函数指针类型后，0就可以看作一个函数的地址 有p时函数指针变量
//	(void(*)())0;//没有p时代表函数指针类型，void表该函数返回类型，()表参数，无参，*表指针，void表返回类型是函数的void，即把0强制转化成函数指针类型，转化后0就是这个类型的一个函数的地址，即0放置着这个函数
//
//	//(*(void(*)())0)//如何调用这个函数？先解引用，再整体括起来，相当于找到0地址的函数，如何调用
//	(*(void(*)())0)();//再来个括号函数传参，但此函数无参
//	//即调用0地址处的函数，该函数无参，返回类型是void
//	//1.void(*)() - 函数指针类型
//	//2.(void(*)())0 - 对0进行强制类型转换，被解释为一个函数地址
//	//3.*(void(*)())0 - 对0地址进行的解引用操作
//	//4.(*(void(*)())0)() - 调用0地址处的函数
//
//	//出自《C陷阱和缺陷》
//
//	(*(void (*)())0x0012ff48)();//语法上没有任何问题，但任何内存空间的地址分配给谁是由编译器决定的，这次分配40，下次分配48，所以很少对直接的常量作为地址上用，但嵌入式方面、硬件开发是固定的
//	return 0;
//}

//void ( * signal(int, void(*)(int)))(int);

//从名字突破，signal是什么？它后面是()-->signal是个函数名，如void test()

//函数就讨论函数的参数：int,void(*)(int),即整型和函数指针类型

//那如果signal是函数名，参数找到了，返回类型呢？剩下的都是返回类型

//signal(int,void(*)(int))拿掉，只剩void(*)(int),就又是一个函数指针类型

//即signal是个函数名，后面括号是函数参数类型，第一个参数类型是正数类型，第二个是函数指针类型，而函数的返回类型，也是个指针类型
//1.signal先和()先结合，说明signal是函数名
//2.signal函数第一个参数的类型是int，第二个参数类型是函数指针类型，该函数指针，指向一个参数为int，返回类型是void的函数
//3.signal函数的返回类型也是一个函数指针，该函数指针也是一个指向一个参数为int，返回类型是void的函数
//signal是一个函数的声明

//太复杂，简化：
//void(*)(int) signal(int,void(*)(int))//signal是函数名，第一个参数是int，第二个是函数指针类型，他的返回类型也是函数指针类型，这样好理解，但语法不支持
//语法上，如果函数返回类型如果是函数指针，*必须和函数名字放一起

//简化：
//typedef - 对类型进行重定义（重命名）
//typedef void(*)(int) pfun_t;//把指针类型起名为pfun_t,但又不能这样写，名字又要放*后面，即：
//typedef void(*pfun_t)(int);//对void(*)(int)的函数指针类型重命名为pfun_t
//等价于typedef unsigned int uint;
//定义好后就可以替换了
//pfun_t signal(int, pfun_t);//简洁了，完全等价的

//函数指针数组 - 存放函数指针的数组
//整型指针 int*
//整型指针数组 int* arr[5]
//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int main()
//{
//	int (*pf1)(int,int)=Add;
//	int (*pf2)(int,int)=Sub;//函数指针
//	
//	int (*pfArr[2])(int, int) = {Add,Sub};//改成pfArr再加个[2]就可以了
//	//int (*pfArr[2])(int, int) = { pf1,pf2 };//也可以的
//	//pfArr首先和[]结合，说明是数组，有两个元素，剩下的都是元素类型，去掉pfArr[2],剩下int (*)(int,int),就是函数指针
//	//这就是函数指针数组，即写出函数指针，再加[]就是数组了,pfArr就是函数指针数组，可以存放同类型的函数指针
//	return 0;
//}

//怎么用？
//函数指针数组的运用：
//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//void menu()
//{
//	printf("*********************\n");
//	printf("**** 1.add 2.sub ****\n");
//	printf("**** 3.mul 4.div ****\n");
//	printf("****    0.exit   ****\n");
//	printf("*********************\n");
//}
//int main()
//{
//	//实现计算器 - 加、减、乘、除 整数类型
//	int input = 0;
//	do {
//		menu();
//		int x = 0;
//		int y = 0;
//		int ret = 0;
//		printf("请选择：>");
//		scanf("%d",&input);
//		switch (input)
//		{
//		case 1:
//			printf("请输入两个操作数：>");
//			scanf("%d %d", &x, &y);
//			ret = Add(x,y);
//			printf("ret = %d\n", ret);
//			break;
//		case 2:
//			printf("请输入两个操作数：>");
//			scanf("%d %d", &x, &y);
//			ret = Sub(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 3:
//			printf("请输入两个操作数：>");
//			scanf("%d %d", &x, &y);
//
//			ret = Mul(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 4:
//			printf("请输入两个操作数：>");
//			scanf("%d %d", &x, &y);
//			ret = Div(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 0:
//			printf("退出程序\n");
//			break;
//		default:
//			printf("选择错误，重新选择\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}
//代码冗余
//想要更多功能的话，会更冗余，不停＋case
//可以用函数指针的数组方式来解决
//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//void menu()
//{
//	printf("*********************\n");
//	printf("**** 1.add 2.sub ****\n");
//	printf("**** 3.mul 4.div ****\n");
//	printf("****    0.exit   ****\n");
//	printf("*********************\n");
//}
//int main()
//{
//	//实现计算器 - 加、减、乘、除 整数类型
//	int input = 0;
//	do {
//		menu();		
//		//pfArr就是函数指针数组
//		int (*pfArr[5])(int, int) = { NULL, Add,Sub,Mul,Div };//先写函数指针类型，再加[]即可
//		//前提：各函数的参数、返回类型都要统一才行
//		//实现随下标跳转的作用，又叫转移表，出自《c和指针》
//
//		int x = 0;
//		int y = 0;
//		int ret = 0;
//		printf("请选择：>");
//		scanf("%d", &input);
//		if (input >= 1 && input <= 4)
//		{
//			printf("请输入两个操作数：>");
//			scanf("%d %d", &x, &y);
//			ret = (pfArr[input])(x, y);//拿到函数并调用
//			printf("ret = %d\n", ret);
//		}
//		else if(input==0)
//		{
//			printf("退出程序\n");
//			break;
//		}
//		else
//		{
//			printf("选择错误\n");
//		}
//	} while (input);
//	return 0;
//}

//指向函数指针数组的指针：
//函数指针的数组 - 数组   是数组就可以取出地址
//取出函数指针数组的地址

//整型数组：
//int arr[5]
//int (*p1)[5]=& arr;*括起来表指针，[]表指向数组，5表该数组有5个元素，元素类型是int
//整型指针的数组
//int* arr[5];
//int* (*p2)[5]&arr;*括起来表指针，[]表指向数组，5表该数组有5个元素，元素类型是int*指针
//p2是指向整型指针数组的指针

//函数指针的数组：&函数指针数组
//int(*p)(int, int);//函数指针
//int(*p2[4])(int, int);//函数指针的数组
//p3=&p2;//取出的是函数指针数组的地址
////p3就是一个指向函数指针数组的指针，则p3该如何书写呢？
//int(*(*p3)[4])(int, int);//首先括*p3表p3是个指针，和[]结合表p3指向的是一个数组，4表该数组有四个元素，去掉(*p3)[4],剩下int(*)(int,int)为数组元素类型为函数指针
////即p3为函数指针数组的指针  - 了解即可

////定义数组时，把数组名和元素个数去掉就是数组的元素类型
////去掉元素名，剩下的是数组类型
//int main()
//{
//	int arr[10];
//	//数组元素类型就是int
//	//数组也是一种对象，也有自己的类型,arr数组的类型是int[10]
//	return 0;
//}

//回调函数：通过函数指针调用的函数
//A函数   B函数(A函数的地址)必须用A函数的指针来接收，当进入B函数，若通过A函数的指针调用A函数，则称该机制为回调函数机制
//再改造计算器：还用switch
int Add(int x, int y)
{
	return x + y;
}
int Sub(int x, int y)
{
	return x - y;
}
int Mul(int x, int y)
{
	return x * y;
}
int Div(int x, int y)
{
	return x / y;
}

void menu()
{
	printf("*********************\n");
	printf("**** 1.add 2.sub ****\n");
	printf("**** 3.mul 4.div ****\n");
	printf("****    0.exit   ****\n");
	printf("*********************\n");
}

int Calc(int (*pf)(int,int))//传过来函数地址，用函数指针接收
{
	int x = 0;
	int y = 0;
	printf("请输入2个操作数：>");
	scanf("%d %d", &x, &y);
	return pf(x, y);//回调函数机制，通过函数指针调用函数
}
int main()
{
	int input = 0;
	do {
		menu();
		int x = 0;
		int y = 0;
		int ret = 0;
		printf("请选择：>");
		scanf("%d",&input);
		switch (input)
		{
		case 1:
			ret=Calc(Add);
			printf("ret = %d\n", ret);
			break;
		case 2:
			ret = Calc(Sub);
			printf("ret = %d\n", ret);
			break;
		case 3:
			ret = Calc(Mul);
			printf("ret = %d\n", ret);
			break;
		case 4:
			ret = Calc(Div);
			printf("ret = %d\n", ret);
			break;
		case 0:
			printf("退出程序\n");
			break;
		default:
			printf("选择错误，重新选择\n");
			break;
		}
	} while (input);
	return 0;
}
