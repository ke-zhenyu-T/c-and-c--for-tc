#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"
void InitBoard(char board[ROWS][COLS], int rows, int cols,char set)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			board[i][j] = set;//矛盾了'#'怎么办？--再来个参数-->四个参数
		}
	}

}
void DisplayBoard(char board[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;
	printf("--------扫雷游戏--------\n");
	//打印列号
	for (i = 0; i <= col; i++)
	{
		printf("%d ", i);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		printf("%d ", i); //行号
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("--------扫雷游戏--------\n");

}
void SetMine(char mine[ROWS][COLS], int row, int col)
{
	//布置10个雷
	int count = EASY_COUNT;
	while (count)
	{
		//生成随机的下标
		int x = rand()%row+1;
		int y = rand()%col+1;
		if (mine[x][y] == '0')
		{
			mine[x][y] = '1';
			count--;
		}
	}
}
static get_mine_count(char mine[ROWS][COLS], int x, int y)//没放头文件仅仅是因为该函数仅仅时为了支持FindMine函数的实现，不需要暴露到头文件里去
{
	//加上static，只能在本源文件，不会和其他文件函数重名
	// static:1.修饰局部变量；2.修饰全局变量；3.修饰函数
	//遍历周围的八个坐标--x行，y列
	return mine[x - 1][y] +
		mine[x - 1][y - 1] +
		mine[x + 1][y - 1] +
		mine[x + 1][y] +
		mine[x + 1][y + 1] +
		mine[x][y + 1] +
		mine[x][y - 1] +
		mine[x - 1][y + 1] - 8 * '0';/*最简单*/
	//遍历也可以
	//int i = 0;
	//for (i = -1; i <= 1; i++)
	//{
	//	int j = 0;
	//	for (j = -1; j <= 1; j++)
	//	{
	//		mine[x + i][y + i];
	//	}//九种，要说x=0，y=0时不可以
	//}
}
void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	//1.输入排查的坐标
	//2.检查座标处是不是雷
		//(1)是雷--很遗憾炸死了
		//(2)不是雷--统计坐标周围有几个雷--存储排查雷的信息到show数组
	int x = 0;
	int y = 0;	
	int win = 0;
	while (win<row*col- EASY_COUNT)
	{
		
		printf("请输入要排查的坐标:>");
		scanf("%d%d", &x, &y);//x-->(1,9),y-->(1,9)
		//判断坐标合法性

		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			if (mine[x][y] == '1')
			{
				printf("很遗憾，你被炸死了\n");
				DisplayBoard(mine, row, col);
				break;
			}
			else
			{
				//不是雷，就统计（x，y）周围几个雷
				int count = get_mine_count(mine, x, y);
				show[x][y] = count + '0';//count是数字，show里放的是字符，要把数字转换为字符-->ASCII码'0'--48,'1--49',-->+48即可得到字符的ASCII码值，而'0'的ASCII码值就是48，即3+'0'='3'
				//显示排查出的信息
				DisplayBoard(show, row, col);
				win++;
			}
		}
		else
		{
			printf("坐标不合法，请重新输入\n");
		}
	}
	if (win == row * col - EASY_COUNT)
	{
		printf("恭喜你，排雷成功\n");
		DisplayBoard(mine, row, col);
	}
}