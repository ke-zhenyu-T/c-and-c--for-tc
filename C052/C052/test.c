#define _CRT_SECURE_NO_WARNINGS 1
//test.c--扫雷游戏的测试
//game.c--游戏的函数实现
//game.h--游戏的函数声明
#include"game.h"
#include<stdio.h>
void menu()
{
	printf("*********************\n");
	printf("***** 1.play ********\n");
	printf("***** 0.exit ********\n");
	printf("*********************\n");
}
void game()
{
	//9 * 9的 二维数组
	//	1.布置雷--存放雷的棋盘，有雷--1，没雷为--0
	//	2.排查雷--1.1是有雷，还是排查出周围有雷的信息呢？肝，就用0和1显示雷非雷-- > 再来一个额外的数组，专门排查雷的信息
	//	写两个数组：1.存放布置好的雷的信息；2.存放排查出的雷的信息，没排查的显示 * -- > char类型数组，有雷字符0，字符0表非雷
	//	边角界外扫不到-->加大一圈-->11*11
	char mine[ROWS][COLS] = { 0 };//存放布置好的雷的信息
	char show[ROWS][COLS] = { 0 };//存放排查出的雷的信息
	//初始化棋盘
	//第一个数组初始化为：'0'
	//第二个数组初始化为：'*'
	InitBoard(mine, ROWS, COLS,'0');//初始化函数
	InitBoard(show, ROWS, COLS,'*');
	//打印一下棋盘
	
	DisplayBoard(show,ROW,COL);

	//先布置雷-->随机找十个坐标放雷
	SetMine(mine,ROW,COL);
	//DisplayBoard(mine,ROW,COL);

	//再排查雷--从mine里排查出信息放到show
	FindMine(mine,show,ROW,COL);
}
int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));//不强制转化也可以，有警告而已

	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();//扫雷游戏
			break;
		case 0:
			printf("退出游戏");
			break;
		default:
			printf("选择错误，重新选择");
			break;//跳出switch
		}
	} while (input);
	return 0;
}
//可优化：1.x，y这个坐标不是雷，周围也没雷-->展开一片-->函数递归
//2.发现雷，标记
